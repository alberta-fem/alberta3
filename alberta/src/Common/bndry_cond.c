/*******************************************************************************
 * ALBERTA:  an Adaptive multi Level finite element toolbox using
 *           Bisectioning refinement and Error control by Residual
 *           Techniques for scientific Applications
 *
 * file: alberta.h
 *
 * description: support Dirichlet, Neumann and Robin boundary conditions
 *
 *******************************************************************************
 *
 *  This file's authors: Claus-Justus Heine
 *                       Abteilung fuer Angewandte Mathematik
 *                       Universitaet Freiburg
 *                       Hermann-Herder-Str. 10
 *                       79104 Freiburg, Germany
 *
 *  http://www.alberta-fem.de
 *
 *  (c) by C.-J. Heine (2007).
 *
 ******************************************************************************/

#include "alberta_intern.h"
#include "alberta.h"

#if 0
TODO

bool boundary_conditions(const BNDRY_COND_INFO *bci,
			 DOF_MATRIX *matrix,
			 DOF_REAL_VEC *fh,
			 DOF_REAL_VEC *uh,
			 DOF_SCHAR_VEC *bound)
{
  MESH *mesh = bci->fe_space->mesh;
}
#endif
