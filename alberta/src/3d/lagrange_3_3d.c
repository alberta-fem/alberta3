/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     lagrange_3_3d.c                                                */
/*                                                                          */
/* description:  piecewise cubic Lagrange elements in 3d                    */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

static const REAL_B bary3_3d[N_BAS_LAG_3_3D] = {
  {1.0, 0.0, 0.0, 0.0}, /* vertices */
  {0.0, 1.0, 0.0, 0.0},
  {0.0, 0.0, 1.0, 0.0},
  {0.0, 0.0, 0.0, 1.0},
  {2.0/3.0,1.0/3.0,    0.0,    0.0}, /* edges */
  {1.0/3.0,2.0/3.0,    0.0,    0.0},
  {2.0/3.0,    0.0,1.0/3.0,    0.0},
  {1.0/3.0,    0.0,2.0/3.0,    0.0},
  {2.0/3.0,    0.0,    0.0,1.0/3.0},
  {1.0/3.0,    0.0,    0.0,2.0/3.0},
  {    0.0,2.0/3.0,1.0/3.0,    0.0},
  {    0.0,1.0/3.0,2.0/3.0,    0.0},
  {    0.0,2.0/3.0,    0.0,1.0/3.0},
  {    0.0,1.0/3.0,    0.0,2.0/3.0},
  {    0.0,    0.0,2.0/3.0,1.0/3.0},
  {    0.0,    0.0,1.0/3.0,2.0/3.0},
  {    0.0,1.0/3.0,1.0/3.0,1.0/3.0}, /* faces == walls */
  {1.0/3.0,    0.0,1.0/3.0,1.0/3.0},
  {1.0/3.0,1.0/3.0,    0.0,1.0/3.0},
  {1.0/3.0,1.0/3.0,1.0/3.0,    0.0}
};

static LAGRANGE_DATA lag_3_3d_data = {
  bary3_3d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3v0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (4.5*(lambda[0] - 1.0)*lambda[0] + 1.0)*lambda[0];
}

static const REAL *grd_phi3v0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = (13.5*lambda[0] - 9.0)*lambda[0] + 1;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3v0_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 27.0*lambda[0] - 9.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3v0_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][0] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3v1_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (4.5*(lambda[1] - 1.0)*lambda[1] + 1.0)*lambda[1];
}

static const REAL *grd_phi3v1_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[1] = (13.5*lambda[1] - 9.0)*lambda[1] + 1;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3v1_3d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BB D2;
  D2[1][1] = 27.0*lambda[1] - 9.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3v1_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][1] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3v2_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (4.5*(lambda[2] - 1.0)*lambda[2] + 1.0)*lambda[2];
}

static const REAL *grd_phi3v2_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0.0, 0.0, 0.0};
  grd[2] = (13.5*lambda[2] - 9.0)*lambda[2] + 1;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3v2_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = 27.0*lambda[2] - 9.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3v2_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][2] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 3                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3v3_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (4.5*(lambda[3] - 1.0)*lambda[3] + 1.0)*lambda[3];
}

static const REAL *grd_phi3v3_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0.0, 0.0, 0.0};
  grd[3] = (13.5*lambda[3] - 9.0)*lambda[3] + 1;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3v3_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[3][3] = 27.0*lambda[3] - 9.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3v3_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[3][3][3] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 0 inbetween vertex 0 and 1                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[0] - 4.5)*lambda[0]*lambda[1];
}

static const REAL *grd_phi3e0_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[0] = (27.0*lambda[0] - 4.5)*lambda[1];
  grd[1] = (13.5*lambda[0] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e0_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 27.0*lambda[1];
  D2[0][1] = D2[1][0] = 27.0*lambda[0] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e0_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 0 inbetween vertex 0 and 1                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[1] - 4.5)*lambda[1]*lambda[0];
}

static const REAL *grd_phi3e1_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[0] = (13.5*lambda[1] - 4.5)*lambda[1];
  grd[1] = (27.0*lambda[1] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e1_3d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BB D2;
  D2[1][1] = 27.0*lambda[0];
  D2[0][1] = D2[1][0] = 27.0*lambda[1] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e1_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][1][1] = D3[1][1][0] = D3[1][0][1] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 1 inbetween vertex 0 and 2                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e2_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (13.5*lambda[0] - 4.5)*lambda[0]*lambda[2];
}

static const REAL *grd_phi3e2_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[0] = (27.0*lambda[0] - 4.5)*lambda[2];
  grd[2] = (13.5*lambda[0] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e2_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 27.0*lambda[2];
  D2[0][2] = D2[2][0] = 27.0*lambda[0] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e2_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][2] = D3[0][2][0] = D3[2][0][0] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 1 inbetween vertex 0 and 2                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e3_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (13.5*lambda[2] - 4.5)*lambda[2]*lambda[0];
}

static const REAL *grd_phi3e3_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[0] = (13.5*lambda[2] - 4.5)*lambda[2];
  grd[2] = (27.0*lambda[2] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e3_3d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BB D2;
  D2[2][2] = 27.0*lambda[0];
  D2[0][2] = D2[2][0] = 27.0*lambda[2] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e3_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][2][2] = D3[2][2][0] = D3[2][0][2] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 2 inbetween vertex 0 and 3                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e4_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (13.5*lambda[0] - 4.5)*lambda[0]*lambda[3];
}

static const REAL *grd_phi3e4_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[0] = (27.0*lambda[0] - 4.5)*lambda[3];
  grd[3] = (13.5*lambda[0] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e4_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 27.0*lambda[3];
  D2[0][3] = D2[3][0] = 27.0*lambda[0] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e4_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][3] = D3[0][3][0] = D3[3][0][0] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 2 inbetween vertex 0 and 3                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e5_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[3] - 4.5)*lambda[3]*lambda[0];
}

static const REAL *grd_phi3e5_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[0] = (13.5*lambda[3] - 4.5)*lambda[3];
  grd[3] = (27.0*lambda[3] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e5_3d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BB D2;
  D2[3][3] = 27.0*lambda[0];
  D2[0][3] = D2[3][0] = 27.0*lambda[3] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e5_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][3][3] = D3[3][3][0] = D3[3][0][3] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 3 inbetween vertex 1 and 2                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e6_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[1] - 4.5)*lambda[1]*lambda[2];
}

static const REAL *grd_phi3e6_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[1] = (27.0*lambda[1] - 4.5)*lambda[2];
  grd[2] = (13.5*lambda[1] - 4.5)*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e6_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = 27.0*lambda[2];
  D2[1][2] = D2[2][1] = 27.0*lambda[1] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e6_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][2] = D3[1][2][1] = D3[2][1][1] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 3 inbetween vertex 1 and 2                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e7_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (13.5*lambda[2] - 4.5)*lambda[2]*lambda[1];
}

static const REAL *grd_phi3e7_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[1] = (13.5*lambda[2] - 4.5)*lambda[2];
  grd[2] = (27.0*lambda[2] - 4.5)*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e7_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = 27.0*lambda[1];
  D2[1][2] = D2[2][1] = 27.0*lambda[2] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e7_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][2][2] = D3[2][2][1] = D3[2][1][2] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 4 inbetween vertex 1 and 3                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e8_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[1] - 4.5)*lambda[1]*lambda[3];
}

static const REAL *grd_phi3e8_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = (27.0*lambda[1] - 4.5)*lambda[3];
  grd[3] = (13.5*lambda[1] - 4.5)*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e8_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = 27.0*lambda[3];
  D2[1][3] = D2[3][1] = 27.0*lambda[1] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e8_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][3] = D3[1][3][1] = D3[3][1][1] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 4 inbetween vertex 1 and 3                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e9_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (13.5*lambda[3] - 4.5)*lambda[3]*lambda[1];
}

static const REAL *grd_phi3e9_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[1] = (13.5*lambda[3] - 4.5)*lambda[3];
  grd[3] = (27.0*lambda[3] - 4.5)*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e9_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[3][3] = 27.0*lambda[1];
  D2[1][3] = D2[3][1] = 27.0*lambda[3] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e9_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][3][3] = D3[3][3][1] = D3[3][1][3] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 5 inbetween vertex 2 and 3                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e10_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (13.5*lambda[2] - 4.5)*lambda[2]*lambda[3];
}

static const REAL *grd_phi3e10_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[2] = (27.0*lambda[2] - 4.5)*lambda[3];
  grd[3] = (13.5*lambda[2] - 4.5)*lambda[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e10_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = 27.0*lambda[3];
  D2[2][3] = D2[3][2] = 27.0*lambda[2] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e10_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][3] = D3[2][3][2] = D3[3][2][2] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 5 inbetween vertex 2 and 3                      */
/*--------------------------------------------------------------------------*/

static REAL phi3e11_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return (13.5*lambda[3] - 4.5)*lambda[3]*lambda[2];
}

static const REAL *grd_phi3e11_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[2] = (13.5*lambda[3] - 4.5)*lambda[3];
  grd[3] = (27.0*lambda[3] - 4.5)*lambda[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e11_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[3][3] = 27.0*lambda[2];
  D2[2][3] = D2[3][2] = 27.0*lambda[3] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e11_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][3][3] = D3[3][3][2] = D3[3][2][3] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at barycenter of face 0                                   */
/*--------------------------------------------------------------------------*/

static REAL phi3c0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return 27.0*lambda[1]*lambda[2]*lambda[3];
}

static const REAL *grd_phi3c0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = 27.0*lambda[2]*lambda[3];
  grd[2] = 27.0*lambda[1]*lambda[3];
  grd[3] = 27.0*lambda[1]*lambda[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3c0_3d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BB D2;
  D2[1][2] = D2[2][1] = 27.0*lambda[3];
  D2[1][3] = D2[3][1] = 27.0*lambda[2];
  D2[2][3] = D2[3][2] = 27.0*lambda[1];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3c0_3d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BBB D3;

  D3[1][2][3] = D3[1][3][2] = D3[2][1][3] = 
    D3[3][2][1] = D3[3][1][2] = D3[2][3][1] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at barycenter of face 1                                   */
/*--------------------------------------------------------------------------*/

static REAL phi3c1_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return 27.0*lambda[0]*lambda[2]*lambda[3];
}

static const REAL *grd_phi3c1_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd = {0};
  grd[0] = 27.0*lambda[2]*lambda[3];
  grd[2] = 27.0*lambda[0]*lambda[3];
  grd[3] = 27.0*lambda[0]*lambda[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3c1_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][2] = D2[2][0] = 27.0*lambda[3];
  D2[0][3] = D2[3][0] = 27.0*lambda[2];
  D2[2][3] = D2[3][2] = 27.0*lambda[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3c1_3d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BBB D3;

  D3[0][2][3] = D3[0][3][2] = D3[2][0][3] = 
    D3[3][2][0] = D3[3][0][2] = D3[2][3][0] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at barycenter of face 2                                   */
/*--------------------------------------------------------------------------*/

static REAL phi3c2_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return 27.0*lambda[0]*lambda[1]*lambda[3];
}

static const REAL *grd_phi3c2_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd;
  grd[0] = 27.0*lambda[1]*lambda[3];
  grd[1] = 27.0*lambda[0]*lambda[3];
  grd[3] = 27.0*lambda[0]*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3c2_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 27.0*lambda[3];
  D2[0][3] = D2[3][0] = 27.0*lambda[1];
  D2[1][3] = D2[3][1] = 27.0*lambda[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3c2_3d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BBB D3;

  D3[0][1][3] = D3[0][3][1] = D3[1][0][3] = 
    D3[3][1][0] = D3[3][0][1] = D3[1][3][0] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at barycenter of face 3                                   */
/*--------------------------------------------------------------------------*/

static REAL phi3c3_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return 27.0*lambda[0]*lambda[1]*lambda[2];
}

static const REAL *grd_phi3c3_3d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  static REAL_B grd;
  grd[0] = 27.0*lambda[1]*lambda[2];
  grd[1] = 27.0*lambda[0]*lambda[2];
  grd[2] = 27.0*lambda[0]*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3c3_3d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 27.0*lambda[2];
  D2[0][2] = D2[2][0] = 27.0*lambda[1];
  D2[1][2] = D2[2][1] = 27.0*lambda[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3c3_3d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BBB D3;

  D3[0][1][2] = D3[0][2][1] = D3[1][0][2] = 
    D3[2][1][0] = D3[2][0][1] = D3[1][2][0] = 27.0;

  return (const REAL_BB *)D3;
}

/******************************************************************************/

#undef DEF_EL_VEC_3_3D
#define DEF_EL_VEC_3_3D(type, name)					\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_3_3D, N_BAS_LAG_3_3D)

#undef DEFUN_GET_EL_VEC_3_3D
#define DEFUN_GET_EL_VEC_3_3D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  get_##name##3_3d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("get_"#name"3_3d");					\
    static DEF_EL_VEC_3_3D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, ibas, inode, node0;						\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    n0 = admin->n0_dof[VERTEX];						\
    for (ibas = inode = 0; inode < N_VERTICES_3D; inode++, ibas++) {	\
      dof = dofptr[inode][n0]; body;					\
    }									\
    n0 = admin->n0_dof[EDGE];						\
    node0 = admin->mesh->node[EDGE];					\
    for (inode = 0; inode < N_EDGES_3D; inode++) {			\
      if (dofptr[vertex_of_edge_3d[inode][0]][0]			\
	  <								\
	  dofptr[vertex_of_edge_3d[inode][1]][0]) {			\
	dof = dofptr[node0+inode][n0];   body; ibas++;			\
	dof = dofptr[node0+inode][n0+1]; body; ibas++;			\
      } else {								\
	dof = dofptr[node0+inode][n0+1]; body; ibas++;			\
	dof = dofptr[node0+inode][n0];   body; ibas++;			\
      }									\
    }									\
    n0 = admin->n0_dof[FACE];						\
    node0 = admin->mesh->node[FACE];					\
    for (inode = 0; inode < N_FACES_3D; inode++) {			\
      dof = dofptr[node0+inode][n0]; body; ibas++;			\
    }									\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_3_3D
#define DEFUN_GET_EL_DOF_VEC_3_3D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_3_3D(_##name##_vec, type, dv->fe_space->admin,	\
			ASSIGN(dv->vec[dof], rvec[ibas]),		\
			const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  get_##name##_vec3_3d(type##_VEC_TYPE *vec, const EL *el,		\
		       const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
    									\
    if (vec != NULL || vec_loc == NULL) {				\
      return get__##name##_vec3_3d(vec, el, dv);			\
    } else {								\
      get__##name##_vec3_3d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy

#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/*--------------------------------------------------------------------------*/
/*  functions for combining basisfunctions with coefficients                */
/*--------------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_3_3D(dof_indices, DOF, admin,
		      rvec[ibas] = dof,
		      const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

static const EL_BNDRY_VEC *
get_bound3_3d(BNDRY_FLAGS *vec,
	      const EL_INFO *el_info, const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound3_3d");
  static DEF_EL_VEC_3_3D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int i, j, k;

  DEBUG_TEST_FLAG(FILL_BOUND, el_info);

  for (j = i = 0; j < N_VERTICES_3D; j++, i++) {
    BNDRY_FLAGS_CPY(rvec[j], el_info->vertex_bound[i]);
  }
  for (i = 0; i < N_EDGES_3D; j += 2, i++) {
    for (k = 0; k < 2; k++) {
      BNDRY_FLAGS_CPY(rvec[j+k], el_info->edge_bound[i]);
    }
  }
  for (i = 0; i < N_FACES_3D; j++, i++) {
    BNDRY_FLAGS_INIT(rvec[j]);
    BNDRY_FLAGS_SET(rvec[j], el_info->face_bound[i]);
  }

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_3D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_3D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_3D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_3D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_3D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_3D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_3D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL(/**/, 3, 3, N_BAS_LAG_3_3D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL_D(/**/, 3, 3, N_BAS_LAG_3_3D);

GENERATE_INTERPOL_DOW(/**/, 3, 3, N_BAS_LAG_3_3D);

/*--------------------------------------------------------------------------*/
/*  functions for interpolation/ restriction during refinement/coarsening   */
/*--------------------------------------------------------------------------*/

static void real_refine_inter3_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_refine_inter3_3d");
  DOF pd[N_BAS_LAG_3_3D];
  DOF cd[N_BAS_LAG_3_3D];
  EL              *el;
  REAL            *v = NULL;
  DOF             cdi;
  int             i, lr_set, node0, n0;
  U_CHAR          typ;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices3_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_3d(cd, el->child[0], admin, bas_fcts);

  v[cd[3]] = (0.0625*(-v[pd[0]] - v[pd[1]]) + 0.5625*(v[pd[4]] + v[pd[5]]));
  v[cd[8]] = (0.3125*(v[pd[0]] - v[pd[5]]) + 0.0625*v[pd[1]]
	      + 0.937500*v[pd[4]]);
  v[cd[9]] = v[pd[4]];
  v[cd[12]] = (0.0625*(v[pd[0]] + v[pd[1]] - v[pd[4]] - v[pd[5]])
	       + 0.25*(-v[pd[6]] - v[pd[10]])
	       + 0.5*(v[pd[7]] + v[pd[11]] + v[pd[19]]));
  v[cd[13]] = v[pd[19]];
  v[cd[14]] = (0.0625*(v[pd[0]] + v[pd[1]] - v[pd[4]] - v[pd[5]])
	       + 0.25*(-v[pd[8]] - v[pd[12]])
	       + 0.5*(v[pd[9]] + v[pd[13]] + v[pd[18]]));
  v[cd[15]] = (v[pd[18]]);
  v[cd[16]] = (0.0625*(v[pd[0]] + v[pd[1]] - v[pd[4]] - v[pd[5]])
	       + 0.125*(-v[pd[6]]-v[pd[8]]-v[pd[10]]-v[pd[12]])
	       + 0.5*(v[pd[16]] + v[pd[17]])
	       + 0.25*(v[pd[18]] + v[pd[19]]));
  v[cd[17]] = (0.0625*(-v[pd[0]] + v[pd[1]])
	       + 0.1875*(v[pd[4]] - v[pd[5]]) + 0.375*v[pd[8]]
	       - 0.125*v[pd[12]] + 0.75*v[pd[18]]);
  v[cd[18]] = (0.0625*(-v[pd[0]] + v[pd[1]])
	       + 0.1875*(v[pd[4]] - v[pd[5]]) + 0.375*v[pd[6]]
	       - 0.125*v[pd[10]] + 0.75*v[pd[19]]);

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_3d(cd, el->child[1], admin, bas_fcts);

  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    v[cd[8]] = (0.0625*v[pd[0]] + 0.3125*(v[pd[1]] - v[pd[4]])
		+ 0.9375*v[pd[5]]);
    v[cd[9]] = v[pd[5]];
    v[cd[17]] = (0.0625*(v[pd[0]] - v[pd[1]])
		 + 0.1875*(-v[pd[4]] + v[pd[5]])
		 - 0.125*v[pd[6]] + 0.375*v[pd[10]] + 0.75*v[pd[19]]);
    v[cd[18]] = (0.0625*(v[pd[0]] - v[pd[1]])
		 + 0.1875*(-v[pd[4]] + v[pd[5]]) - 0.125*v[pd[8]]
		 + 0.375*v[pd[12]] + 0.75*v[pd[18]]);
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    v[cd[8]] = (0.0625*v[pd[0]] + 0.3125*(v[pd[1]] - v[pd[4]])
		+ 0.9375*v[pd[5]]);
    v[cd[9]] = v[pd[5]];
    v[cd[17]] = (0.0625*(v[pd[0]] - v[pd[1]])
		 + 0.1875*(-v[pd[4]] + v[pd[5]]) - 0.125*v[pd[8]]
		 + 0.375*v[pd[12]] + 0.75*v[pd[18]]);
    v[cd[18]] = (0.0625*(v[pd[0]] - v[pd[1]]) +
		 0.1875*(-v[pd[4]] + v[pd[5]]) - 0.125*v[pd[6]]
		 + 0.375*v[pd[10]] + 0.75*v[pd[19]]);
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  node0 = drv->fe_space->admin->mesh->node[FACE];
  n0 = admin->n0_dof[FACE];

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;
    get_dof_indices3_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices3_3d(cd, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      v[cd[12]] = (0.0625*(v[pd[0]]+v[pd[1]]-v[pd[4]]-v[pd[5]])
		   + 0.25*(-v[pd[6]] - v[pd[10]])
		   + 0.5*(v[pd[7]] + v[pd[11]] + v[pd[19]]));
      v[cd[13]] = v[pd[19]];
      v[cd[16]] = (0.0625*(v[pd[0]]+v[pd[1]]-v[pd[4]]-v[pd[5]])
		   + 0.125*(-v[pd[6]]-v[pd[8]]-v[pd[10]]-v[pd[12]])
		   + 0.5*(v[pd[16]] + v[pd[17]])
		   + 0.25*(v[pd[18]] + v[pd[19]]));
      v[cd[18]] = (0.0625*(-v[pd[0]] + v[pd[1]])
		   + 0.1875*(v[pd[4]] - v[pd[5]]) + 0.375*v[pd[6]]
		   - 0.125*v[pd[10]] + 0.75*v[pd[19]]);
      break;
    case 2:
      v[cd[14]] = (0.0625*(v[pd[0]]+v[pd[1]]-v[pd[4]]-v[pd[5]])
		   + 0.25*(-v[pd[8]] - v[pd[12]])
		   + 0.5*(v[pd[9]] + v[pd[13]] + v[pd[18]]));
      v[cd[15]] = v[pd[18]];
      v[cd[16]] = (0.0625*(v[pd[0]]+v[pd[1]]-v[pd[4]]-v[pd[5]])
		   + 0.125*(-v[pd[6]]-v[pd[8]]-v[pd[10]]-v[pd[12]])
		   + 0.5*(v[pd[16]] + v[pd[17]])
		   + 0.25*(v[pd[18]] + v[pd[19]]));
      v[cd[17]] = (0.0625*(-v[pd[0]] + v[pd[1]])
		   + 0.1875*(v[pd[4]] - v[pd[5]]) + 0.375*v[pd[8]]
		   - 0.125*v[pd[12]] + 0.75*v[pd[18]]);
      break;
   case 3:
      v[cd[16]] = (0.0625*(v[pd[0]]+v[pd[1]]-v[pd[4]]-v[pd[5]])
		   + 0.125*(-v[pd[6]]-v[pd[8]]-v[pd[10]]-v[pd[12]])
		   + 0.5*(v[pd[16]] + v[pd[17]])
		   + 0.25*(v[pd[18]] + v[pd[19]]));
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices3_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	cdi = el->child[1]->dof[node0+1][n0];
	DEBUG_TEST_EXIT(cdi == cd[17], "cdi != cd[17]\n");
	v[cdi] = (0.0625*(v[pd[0]] - v[pd[1]])
		  + 0.1875*(-v[pd[4]] + v[pd[5]]) - 0.125*v[pd[6]]
		  + 0.375*v[pd[10]] + 0.75*v[pd[19]]);
	break;
      case 2:
	cdi = el->child[1]->dof[node0+2][n0];
	DEBUG_TEST_EXIT(cdi == cd[18], "cdi != cd[18]\n");
	v[cdi] = (0.0625*(v[pd[0]] - v[pd[1]])
		  + 0.1875*(-v[pd[4]] + v[pd[5]]) - 0.125*v[pd[8]]
		  + 0.375*v[pd[12]] + 0.75*v[pd[18]]);
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	cdi = el->child[1]->dof[node0+2][n0];
	DEBUG_TEST_EXIT(cdi == cd[18], "cdi != cd[18]\n");
	v[cdi] = (0.0625*(v[pd[0]] - v[pd[1]])
		  + 0.1875*(-v[pd[4]] + v[pd[5]]) - 0.125*v[pd[6]]
		  + 0.375*v[pd[10]] + 0.75*v[pd[19]]);
	break;
      case 2:
	cdi = el->child[1]->dof[node0+1][n0];
	DEBUG_TEST_EXIT(cdi == cd[17], "cdi != cd[17]\n");
	v[cdi] = (0.0625*(v[pd[0]] - v[pd[1]])
		  + 0.1875*(-v[pd[4]] + v[pd[5]]) - 0.125*v[pd[8]]
		  + 0.375*v[pd[12]] + 0.75*v[pd[18]]);
	break;
      }
    }
  }

  return;
}

static void real_coarse_inter3_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_inter3_3d");
  DOF pd_o[N_BAS_LAG_3_3D];
  EL        *el;
  REAL      *v = NULL;
  int       i, lr_set;
  int       node_e, node_f, n0_e, n0_f;
  DOF       **pds, **cds;
  DOF       cd, pd;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  node_e = drv->fe_space->admin->mesh->node[EDGE];
  n0_e = admin->n0_dof[EDGE];
  node_f = drv->fe_space->admin->mesh->node[FACE];
  n0_f = admin->n0_dof[FACE];

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/
  pds = el->dof;
  cds = el->child[0]->dof;
  get_dof_indices3_3d(pd_o, el, admin, bas_fcts);

  pd = (pds[0][0] < pds[1][0]) ? pds[node_e][n0_e] : pds[node_e][n0_e+1];
  cd = cds[0][0] < cds[3][0] ? cds[node_e+2][n0_e+1] : cds[node_e+2][n0_e];
  v[pd] = v[cd];

  pd = el->dof[node_f+2][n0_f];
  cd = cds[2][0] < cds[3][0] ? cds[node_e+5][n0_e+1] : cds[node_e+5][n0_e];
  v[pd] = v[cd];

  pd = el->dof[node_f+3][n0_f];
  cd = cds[1][0] < cds[3][0] ? cds[node_e+4][n0_e+1] : cds[node_e+4][n0_e];
  v[pd] = v[cd];

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  cds = el->child[1]->dof;

  pd = (pds[0][0] < pds[1][0]) ? pds[node_e][n0_e+1] : pds[node_e][n0_e];
  cd = cds[0][0] < cds[3][0] ? cds[node_e+2][n0_e+1] : cds[node_e+2][n0_e];
  v[pd] = v[cd];

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;

    pds = el->dof;
    cds = el->child[0]->dof;

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

    switch(lr_set)
    {
    case 1:
      pd = el->dof[node_f+3][n0_f];
      cd = cds[1][0] < cds[3][0] ?
	cds[node_e+4][n0_e+1] : cds[node_e+4][n0_e];
      v[pd] = v[cd];
      break;
    case 2:
      pd = el->dof[node_f+2][n0_f];
      cd = cds[2][0] < cds[3][0] ?
	cds[node_e+5][n0_e+1] : cds[node_e+5][n0_e];
      v[pd] = v[cd];
      break;
    }
  }
  return;
}

static void real_coarse_restr3_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_restr3_3d");
  DOF pd[N_BAS_LAG_3_3D];
  DOF cd[N_BAS_LAG_3_3D];
  EL              *el;
  REAL            *v = NULL;
  DOF             cdi;
  int             node0, n0, i, lr_set;
  U_CHAR          typ;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices3_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_3d(cd, el->child[0], admin, bas_fcts);

  v[pd[0]] += (0.0625*(-v[cd[3]] + v[cd[12]] + v[cd[14]]
		       + v[cd[16]] - v[cd[17]] - v[cd[18]])
	       + 0.3125*v[cd[8]]);
  v[pd[1]] += (0.0625*(-v[cd[3]] + v[cd[8]] + v[cd[12]] + v[cd[14]]
		       + v[cd[16]] + v[cd[17]] + v[cd[18]]));
  v[pd[4]] = (v[cd[9]] + 0.5625*v[cd[3]] + 0.9375*v[cd[8]]
	      + 0.0625*(-v[cd[12]] - v[cd[14]] - v[cd[16]])
	      + 0.1875*(v[cd[17]] + v[cd[18]]));
  v[pd[5]] = (0.5625*v[cd[3]] - 0.3125*v[cd[8]]
	      + 0.0625*(-v[cd[12]] - v[cd[14]] - v[cd[16]])
	      + 0.1875*(-v[cd[17]] - v[cd[18]]));
  v[pd[6]] += (-0.25*v[cd[12]] - 0.125*v[cd[16]] + 0.375*v[cd[18]]);
  v[pd[7]] += 0.5*v[cd[12]];
  v[pd[8]] += (-0.25*v[cd[14]] - 0.125*v[cd[16]] + 0.375*v[cd[17]]);
  v[pd[9]] += 0.5*v[cd[14]];
  v[pd[10]] += (-0.25*v[cd[12]] + 0.125*(-v[cd[16]] - v[cd[18]]));
  v[pd[11]] += 0.5*v[cd[12]];
  v[pd[12]] += (-0.25*v[cd[14]] + 0.125*(-v[cd[16]] - v[cd[17]]));
  v[pd[13]] += 0.5*v[cd[14]];
  v[pd[16]] += 0.5*v[cd[16]];
  v[pd[17]] += 0.5*v[cd[16]];
  v[pd[18]] = (v[cd[15]] + 0.5*v[cd[14]] + 0.25*v[cd[16]]
		 + 0.75*v[cd[17]]);
  v[pd[19]] = (v[cd[13]] + 0.5*v[cd[12]] + 0.25*v[cd[16]]
		 + 0.75*v[cd[18]]);

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_3d(cd, el->child[1], admin, bas_fcts);

  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    v[pd[0]] += 0.0625*(v[cd[8]] + v[cd[17]] + v[cd[18]]);
    v[pd[1]] += 0.3125*v[cd[8]] + 0.0625*(-v[cd[17]] - v[cd[18]]);
    v[pd[4]] += -0.3125*v[cd[8]] + 0.1875*(-v[cd[17]] - v[cd[18]]);
    v[pd[5]] += (v[cd[9]] + 0.9375*v[cd[8]] + 0.1875*(v[cd[17]] + v[cd[18]]));
    v[pd[6]] += -0.125*v[cd[17]];
    v[pd[8]] += -0.125*v[cd[18]];
    v[pd[10]] += 0.375*v[cd[17]];
    v[pd[12]] += 0.375*v[cd[18]];
    v[pd[18]] += 0.75*v[cd[18]];
    v[pd[19]] += 0.750000*v[cd[17]];
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 1|2                                                   */
/*--------------------------------------------------------------------------*/

    v[pd[0]] += 0.0625*(v[cd[8]] + v[cd[17]] + v[cd[18]]);
    v[pd[1]] += 0.3125*v[cd[8]] + 0.0625*(-v[cd[17]] - v[cd[18]]);
    v[pd[4]] += -0.3125*v[cd[8]] + 0.1875*(-v[cd[17]] - v[cd[18]]);
    v[pd[5]] += (v[cd[9]] + 0.9375*v[cd[8]] + 0.1875*(v[cd[17]] + v[cd[18]]));
    v[pd[6]] += -0.125*v[cd[18]];
    v[pd[8]] += -0.125*v[cd[17]];
    v[pd[10]] += 0.375*v[cd[18]];
    v[pd[12]] += 0.375*v[cd[17]];
    v[pd[18]] += 0.75*v[cd[17]];
    v[pd[19]] += 0.75*v[cd[18]];
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  node0 = drv->fe_space->admin->mesh->node[FACE];
  n0 = admin->n0_dof[FACE];

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;

    get_dof_indices3_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices3_3d(cd, el->child[0], admin, bas_fcts);
    cdi = cd[16];

    switch(lr_set)
    {
    case 1:
      v[pd[0]] += 0.0625*(v[cd[12]] + v[cdi] - v[cd[18]]);
      v[pd[1]] += 0.0625*(v[cd[12]] + v[cdi] + v[cd[18]]);
      v[pd[4]] += (0.0625*(-v[cd[12]] - v[cdi]) + 0.1875*v[cd[18]]);
      v[pd[5]] += (0.0625*(-v[cd[12]] - v[cdi]) - 0.1875*v[cd[18]]);
      v[pd[6]] += (-0.25*v[cd[12]] - 0.125*v[cdi] + 0.375*v[cd[18]]);
      v[pd[7]] += 0.5*v[cd[12]];
      v[pd[8]] += -0.125*v[cdi];
      v[pd[10]] += -0.25*v[cd[12]] + 0.125*(-v[cdi] - v[cd[18]]);
      v[pd[11]] += 0.5*v[cd[12]];
      v[pd[12]] += -0.125*v[cdi];
      v[pd[16]] += 0.5*v[cdi];
      v[pd[17]] += 0.5*v[cdi];
      v[pd[18]] += 0.25*v[cdi];
      v[pd[19]] = (v[cd[13]] + 0.5*v[cd[12]] + 0.25*v[cdi] + 0.75*v[cd[18]]);
      break;
    case 2:
      v[pd[0]] += 0.0625*(v[cd[14]] + v[cdi] - v[cd[17]]);
      v[pd[1]] += 0.0625*(v[cd[14]] + v[cdi] + v[cd[17]]);
      v[pd[4]] += 0.0625*(-v[cd[14]] - v[cdi]) + 0.1875*v[cd[17]];
      v[pd[5]] += 0.0625*(-v[cd[14]] - v[cdi]) - 0.1875*v[cd[17]];
      v[pd[6]] += -0.125*v[cdi];
      v[pd[8]] += -0.25*v[cd[14]] - 0.125*v[cdi] + 0.375*v[cd[17]];
      v[pd[9]] += 0.5*v[cd[14]];
      v[pd[10]] += -0.125*v[cdi];
      v[pd[12]] += -0.25*v[cd[14]] + 0.125*(-v[cdi] - v[cd[17]]);
      v[pd[13]] += 0.5*v[cd[14]];
      v[pd[16]] += 0.5*v[cdi];
      v[pd[17]] += 0.5*v[cdi];
      v[pd[18]] = (v[cd[15]] + 0.5*v[cd[14]] + 0.25*v[cdi] + 0.75*v[cd[17]]);
      v[pd[19]] += 0.25*v[cdi];
      break;
    case 3:
      v[pd[0]] += 0.0625*v[cdi];
      v[pd[1]] += 0.0625*v[cdi];
      v[pd[4]] += -0.0625*v[cdi];
      v[pd[5]] += -0.0625*v[cdi];
      v[pd[6]] += -0.125*v[cdi];
      v[pd[8]] += -0.125*v[cdi];
      v[pd[10]] += -0.125*v[cdi];
      v[pd[12]] += -0.125*v[cdi];
      v[pd[16]] += 0.5*v[cdi];
      v[pd[17]] += 0.5*v[cdi];
      v[pd[18]] += 0.25*v[cdi];
      v[pd[19]] += 0.25*v[cdi];
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices3_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	cdi = el->child[1]->dof[node0+1][n0];
	DEBUG_TEST_EXIT(cdi == cd[17], "cdi != cd[17]\n");
	v[pd[0]] += 0.0625*v[cdi];
	v[pd[1]] += -0.0625*v[cdi];
	v[pd[4]] += -0.1875*v[cdi];
	v[pd[5]] += 0.1875*v[cdi];
	v[pd[6]] += -0.125*v[cdi];
	v[pd[10]] += 0.375*v[cdi];
	v[pd[19]] += 0.75*v[cdi];
	break;
      case 2:
	cdi = el->child[1]->dof[node0+2][n0];
	DEBUG_TEST_EXIT(cdi == cd[18], "cdi != cd[18]\n");
	v[pd[0]] += 0.0625*v[cdi];
	v[pd[1]] += -0.0625*v[cdi];
	v[pd[4]] += -0.1875*v[cdi];
	v[pd[5]] += 0.1875*v[cdi];
	v[pd[8]] += -0.125*v[cdi];
	v[pd[12]] += 0.375*v[cdi];
	v[pd[18]] += 0.75*v[cdi];
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	cdi = el->child[1]->dof[node0+2][n0];
	DEBUG_TEST_EXIT(cdi == cd[18], "cdi != cd[18]\n");
	v[pd[0]] += 0.0625*v[cdi];
	v[pd[1]] += -0.0625*v[cdi];
	v[pd[4]] += -0.1875*v[cdi];
	v[pd[5]] += 0.1875*v[cdi];
	v[pd[6]] += -0.125*v[cdi];
	v[pd[10]] += 0.375*v[cdi];
	v[pd[19]] += 0.75*v[cdi];
	break;
      case 2:
	cdi = el->child[1]->dof[node0+1][n0];
	DEBUG_TEST_EXIT(cdi == cd[17], "cdi != cd[17]\n");
	v[pd[0]] += 0.0625*v[cdi];
	v[pd[1]] += -0.0625*v[cdi];
	v[pd[4]] += -0.1875*v[cdi];
	v[pd[5]] += 0.1875*v[cdi];
	v[pd[8]] += -0.125*v[cdi];
	v[pd[12]] += 0.375*v[cdi];
	v[pd[18]] += 0.75*v[cdi];
	break;
      }
    }
  }
  return;
}


static void real_d_refine_inter3_3d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_refine_inter3_3d");
  DOF pd[N_BAS_LAG_3_3D];
  DOF cd[N_BAS_LAG_3_3D];
  EL              *el;
  REAL_D          *v = NULL;
  DOF             cdi;
  int             node0, n0, i, lr_set, k;
  U_CHAR          typ;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices3_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_3d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[cd[3]][k] = (0.0625*(-v[pd[0]][k] - v[pd[1]][k])
		   + 0.5625*(v[pd[4]][k] + v[pd[5]][k]));
    v[cd[8]][k] = (0.3125*(v[pd[0]][k] - v[pd[5]][k]) + 0.0625*v[pd[1]][k]
		   + 0.937500*v[pd[4]][k]);
    v[cd[9]][k] = v[pd[4]][k];
    v[cd[12]][k] = (0.0625*(v[pd[0]][k] + v[pd[1]][k]
			    - v[pd[4]][k] - v[pd[5]][k])
		    + 0.25*(-v[pd[6]][k] - v[pd[10]][k])
		    + 0.5*(v[pd[7]][k] + v[pd[11]][k] + v[pd[19]][k]));
    v[cd[13]][k] = v[pd[19]][k];
    v[cd[14]][k] = (0.0625*(v[pd[0]][k] + v[pd[1]][k] - v[pd[4]][k]
			    - v[pd[5]][k])
		    + 0.25*(-v[pd[8]][k] - v[pd[12]][k])
		    + 0.5*(v[pd[9]][k] + v[pd[13]][k] + v[pd[18]][k]));
    v[cd[15]][k] = (v[pd[18]][k]);
    v[cd[16]][k] = (0.0625*(v[pd[0]][k] + v[pd[1]][k]
			    - v[pd[4]][k] - v[pd[5]][k])
		    + 0.125*(-v[pd[6]][k] - v[pd[8]][k]
			     - v[pd[10]][k] - v[pd[12]][k])
		    + 0.5*(v[pd[16]][k] + v[pd[17]][k])
		    + 0.25*(v[pd[18]][k] + v[pd[19]][k]));
    v[cd[17]][k] = (0.0625*(-v[pd[0]][k] + v[pd[1]][k])
		    + 0.1875*(v[pd[4]][k] - v[pd[5]][k]) + 0.375*v[pd[8]][k]
		    - 0.125*v[pd[12]][k] + 0.75*v[pd[18]][k]);
    v[cd[18]][k] = (0.0625*(-v[pd[0]][k] + v[pd[1]][k])
		    + 0.1875*(v[pd[4]][k] - v[pd[5]][k]) + 0.375*v[pd[6]][k]
		    - 0.125*v[pd[10]][k] + 0.75*v[pd[19]][k]);
  }
/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_3d(cd, el->child[1], admin, bas_fcts);

  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[cd[8]][k] = (0.0625*v[pd[0]][k] + 0.3125*(v[pd[1]][k] - v[pd[4]][k])
		     + 0.9375*v[pd[5]][k]);
      v[cd[9]][k] = v[pd[5]][k];
      v[cd[17]][k] = (0.0625*(v[pd[0]][k] - v[pd[1]][k])
		      + 0.1875*(-v[pd[4]][k] + v[pd[5]][k])
		      - 0.125*v[pd[6]][k] + 0.375*v[pd[10]][k]
		      + 0.75*v[pd[19]][k]);
      v[cd[18]][k] = (0.0625*(v[pd[0]][k] - v[pd[1]][k])
		      + 0.1875*(-v[pd[4]][k] + v[pd[5]][k])
		      - 0.125*v[pd[8]][k]
		      + 0.375*v[pd[12]][k] + 0.75*v[pd[18]][k]);
    }
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[cd[8]][k] = (0.0625*v[pd[0]][k] + 0.3125*(v[pd[1]][k] - v[pd[4]][k])
		     + 0.9375*v[pd[5]][k]);
      v[cd[9]][k] = v[pd[5]][k];
      v[cd[17]][k] = (0.0625*(v[pd[0]][k] - v[pd[1]][k])
		      + 0.1875*(-v[pd[4]][k] + v[pd[5]][k])
		      - 0.125*v[pd[8]][k] + 0.375*v[pd[12]][k]
		      + 0.75*v[pd[18]][k]);
      v[cd[18]][k] = (0.0625*(v[pd[0]][k] - v[pd[1]][k]) +
		      0.1875*(-v[pd[4]][k] + v[pd[5]][k]) - 0.125*v[pd[6]][k]
		      + 0.375*v[pd[10]][k] + 0.75*v[pd[19]][k]);
    }
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  node0 = drdv->fe_space->admin->mesh->node[FACE];
  n0 = admin->n0_dof[FACE];

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;

    get_dof_indices3_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set,"no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices3_3d(cd, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[cd[12]][k] = (0.0625*(v[pd[0]][k]+v[pd[1]][k]
				-v[pd[4]][k]-v[pd[5]][k])
			+ 0.25*(-v[pd[6]][k] - v[pd[10]][k])
			+ 0.5*(v[pd[7]][k] + v[pd[11]][k] + v[pd[19]][k]));
	v[cd[13]][k] = v[pd[19]][k];
	v[cd[16]][k] = (0.0625*(v[pd[0]][k]+v[pd[1]][k]
				-v[pd[4]][k]-v[pd[5]][k])
			+ 0.125*(-v[pd[6]][k]-v[pd[8]][k]
				 -v[pd[10]][k]-v[pd[12]][k])
			+ 0.5*(v[pd[16]][k] + v[pd[17]][k])
			+ 0.25*(v[pd[18]][k] + v[pd[19]][k]));
	v[cd[18]][k] = (0.0625*(-v[pd[0]][k] + v[pd[1]][k])
			+ 0.1875*(v[pd[4]][k] - v[pd[5]][k])
			+ 0.375*v[pd[6]][k] - 0.125*v[pd[10]][k]
			+ 0.75*v[pd[19]][k]);
      }
      break;
    case 2:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[cd[14]][k] = (0.0625*(v[pd[0]][k]+v[pd[1]][k]
				-v[pd[4]][k]-v[pd[5]][k])
			+ 0.25*(-v[pd[8]][k] - v[pd[12]][k])
			+ 0.5*(v[pd[9]][k] + v[pd[13]][k] + v[pd[18]][k]));
	v[cd[15]][k] = v[pd[18]][k];
	v[cd[16]][k] = (0.0625*(v[pd[0]][k]+v[pd[1]][k]
				-v[pd[4]][k]-v[pd[5]][k])
			+ 0.125*(-v[pd[6]][k]-v[pd[8]][k]
				 -v[pd[10]][k]-v[pd[12]][k])
			+ 0.5*(v[pd[16]][k] + v[pd[17]][k])
			+ 0.25*(v[pd[18]][k] + v[pd[19]][k]));
	v[cd[17]][k] = (0.0625*(-v[pd[0]][k] + v[pd[1]][k])
			+ 0.1875*(v[pd[4]][k] - v[pd[5]][k])
			+ 0.375*v[pd[8]][k] - 0.125*v[pd[12]][k]
			+ 0.75*v[pd[18]][k]);
      }
      break;
    case 3:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[cd[16]][k] = (0.0625*(v[pd[0]][k]+v[pd[1]][k]
				-v[pd[4]][k]-v[pd[5]][k])
			+ 0.125*(-v[pd[6]][k]-v[pd[8]][k]
				 -v[pd[10]][k]-v[pd[12]][k])
			+ 0.5*(v[pd[16]][k] + v[pd[17]][k])
			+ 0.25*(v[pd[18]][k] + v[pd[19]][k]));
      }
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices3_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	cdi = el->child[1]->dof[node0+1][n0];
	DEBUG_TEST_EXIT(cdi == cd[17], "cdi != cd[17]\n");
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cdi][k] = (0.0625*(v[pd[0]][k] - v[pd[1]][k])
		       + 0.1875*(-v[pd[4]][k] + v[pd[5]][k])
		       - 0.125*v[pd[6]][k] + 0.375*v[pd[10]][k]
		       + 0.75*v[pd[19]][k]);
	}
	break;
      case 2:
	cdi = el->child[1]->dof[node0+2][n0];
	DEBUG_TEST_EXIT(cdi == cd[18], "cdi != cd[18]\n");
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cdi][k] = (0.0625*(v[pd[0]][k] - v[pd[1]][k])
		       + 0.1875*(-v[pd[4]][k] + v[pd[5]][k])
		       - 0.125*v[pd[8]][k]
		       + 0.375*v[pd[12]][k] + 0.75*v[pd[18]][k]);
	}
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	cdi = el->child[1]->dof[node0+2][n0];
	DEBUG_TEST_EXIT(cdi == cd[18], "cdi != cd[18]\n");
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cdi][k] = (0.0625*(v[pd[0]][k] - v[pd[1]][k])
		       + 0.1875*(-v[pd[4]][k] + v[pd[5]][k])
		       - 0.125*v[pd[6]][k] + 0.375*v[pd[10]][k]
		       + 0.75*v[pd[19]][k]);
	}
	break;
      case 2:
	cdi = el->child[1]->dof[node0+1][n0];
	DEBUG_TEST_EXIT(cdi == cd[17], "cdi != cd[17]\n");
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cdi][k] = (0.0625*(v[pd[0]][k] - v[pd[1]][k])
		       + 0.1875*(-v[pd[4]][k] + v[pd[5]][k])
		       - 0.125*v[pd[8]][k] + 0.375*v[pd[12]][k]
		       + 0.75*v[pd[18]][k]);
	}
	break;
      }
    }
  }

  return;
}

static void real_d_coarse_inter3_3d(DOF_REAL_D_VEC *drdv,
				    RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_inter3_3d");
  DOF pd_o[N_BAS_LAG_3_3D];
  EL        *el;
  REAL_D    *v = NULL;
  int       i, lr_set, k;
  int       node_e, node_f, n0_e, n0_f;
  DOF       **pds, **cds, cd, pd;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  node_e = drdv->fe_space->admin->mesh->node[EDGE];
  n0_e = admin->n0_dof[EDGE];
  node_f = drdv->fe_space->admin->mesh->node[FACE];
  n0_f = admin->n0_dof[FACE];

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/
  pds = el->dof;
  cds = el->child[0]->dof;
  get_dof_indices3_3d(pd_o, el, admin, bas_fcts);

  pd = (pds[0][0] < pds[1][0]) ? pds[node_e][n0_e] : pds[node_e][n0_e+1];
  cd = cds[0][0] < cds[3][0] ? cds[node_e+2][n0_e+1] : cds[node_e+2][n0_e];
  for (k = 0; k < DIM_OF_WORLD; k++)
    v[pd][k] = v[cd][k];

  pd = el->dof[node_f+2][n0_f];
  cd = cds[2][0] < cds[3][0] ? cds[node_e+5][n0_e+1] : cds[node_e+5][n0_e];
  for (k = 0; k < DIM_OF_WORLD; k++)
    v[pd][k] = v[cd][k];

  pd = el->dof[node_f+3][n0_f];
  cd = cds[1][0] < cds[3][0] ? cds[node_e+4][n0_e+1] : cds[node_e+4][n0_e];
  for (k = 0; k < DIM_OF_WORLD; k++)
    v[pd][k] = v[cd][k];

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  cds = el->child[1]->dof;

  pd = (pds[0][0] < pds[1][0]) ? pds[node_e][n0_e+1] : pds[node_e][n0_e];
  cd = cds[0][0] < cds[3][0] ? cds[node_e+2][n0_e+1] : cds[node_e+2][n0_e];
  for (k = 0; k < DIM_OF_WORLD; k++)
    v[pd][k] = v[cd][k];


/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;

    pds = el->dof;
    cds = el->child[0]->dof;

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

    switch(lr_set)
    {
    case 1:
      pd = el->dof[node_f+3][n0_f];
      cd = cds[1][0] < cds[3][0] ?
	cds[node_e+4][n0_e+1] : cds[node_e+4][n0_e];
      for (k = 0; k < DIM_OF_WORLD; k++)
	v[pd][k] = v[cd][k];
      break;
    case 2:
      pd = el->dof[node_f+2][n0_f];
      cd = cds[2][0] < cds[3][0] ?
	cds[node_e+5][n0_e+1] : cds[node_e+5][n0_e];
      for (k = 0; k < DIM_OF_WORLD; k++)
	v[pd][k] = v[cd][k];
      break;
    }
  }
  return;
}

static void real_d_coarse_restr3_3d(DOF_REAL_D_VEC *drdv,
				    RC_LIST_EL *list, int n)
{
  FUNCNAME("real_d_coarse_restr3_3d");
  DOF pd[N_BAS_LAG_3_3D];
  DOF cd[N_BAS_LAG_3_3D];
  EL              *el;
  REAL_D          *v = NULL;
  DOF             cdi;
  int             node0, n0, i, k, lr_set;
  U_CHAR          typ;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices3_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_3d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[0]][k] += (0.0625*(-v[cd[3]][k] + v[cd[12]][k] + v[cd[14]][k]
			    + v[cd[16]][k] - v[cd[17]][k] - v[cd[18]][k])
		    + 0.3125*v[cd[8]][k]);
    v[pd[1]][k] += (0.0625*(-v[cd[3]][k] + v[cd[8]][k] + v[cd[12]][k]
			    + v[cd[14]][k] + v[cd[16]][k] + v[cd[17]][k]
			    + v[cd[18]][k]));
    v[pd[4]][k] = (v[cd[9]][k] + 0.5625*v[cd[3]][k] + 0.9375*v[cd[8]][k]
		   + 0.0625*(-v[cd[12]][k] - v[cd[14]][k] - v[cd[16]][k])
		   + 0.1875*(v[cd[17]][k] + v[cd[18]][k]));
    v[pd[5]][k] = (0.5625*v[cd[3]][k] - 0.3125*v[cd[8]][k]
		   + 0.0625*(-v[cd[12]][k] - v[cd[14]][k] - v[cd[16]][k])
		   + 0.1875*(-v[cd[17]][k] - v[cd[18]][k]));
    v[pd[6]][k] += (-0.25*v[cd[12]][k] - 0.125*v[cd[16]][k]
		    + 0.375*v[cd[18]][k]);
    v[pd[7]][k] += 0.5*v[cd[12]][k];
    v[pd[8]][k] += (-0.25*v[cd[14]][k] - 0.125*v[cd[16]][k]
		    + 0.375*v[cd[17]][k]);
    v[pd[9]][k] += 0.5*v[cd[14]][k];
    v[pd[10]][k] += (-0.25*v[cd[12]][k]
		     + 0.125*(-v[cd[16]][k] - v[cd[18]][k]));
    v[pd[11]][k] += 0.5*v[cd[12]][k];
    v[pd[12]][k] += (-0.25*v[cd[14]][k]
		     + 0.125*(-v[cd[16]][k] - v[cd[17]][k]));
    v[pd[13]][k] += 0.5*v[cd[14]][k];
    v[pd[16]][k] += 0.5*v[cd[16]][k];
    v[pd[17]][k] += 0.5*v[cd[16]][k];
    v[pd[18]][k] = (v[cd[15]][k] + 0.5*v[cd[14]][k] + 0.25*v[cd[16]][k]
		    + 0.75*v[cd[17]][k]);
    v[pd[19]][k] = (v[cd[13]][k] + 0.5*v[cd[12]][k] + 0.25*v[cd[16]][k]
		    + 0.75*v[cd[18]][k]);
  }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_3d(cd, el->child[1], admin, bas_fcts);

  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[pd[0]][k] += 0.0625*(v[cd[8]][k] + v[cd[17]][k] + v[cd[18]][k]);
      v[pd[1]][k] += (0.3125*v[cd[8]][k]
		      + 0.0625*(-v[cd[17]][k] - v[cd[18]][k]));
      v[pd[4]][k] += (-0.3125*v[cd[8]][k]
		      + 0.1875*(-v[cd[17]][k] - v[cd[18]][k]));
      v[pd[5]][k] += (v[cd[9]][k] + 0.9375*v[cd[8]][k]
		      + 0.1875*(v[cd[17]][k] + v[cd[18]][k]));
      v[pd[6]][k] += -0.125*v[cd[17]][k];
      v[pd[8]][k] += -0.125*v[cd[18]][k];
      v[pd[10]][k] += 0.375*v[cd[17]][k];
      v[pd[12]][k] += 0.375*v[cd[18]][k];
      v[pd[18]][k] += 0.75*v[cd[18]][k];
      v[pd[19]][k] += 0.750000*v[cd[17]][k];
    }
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 1|2                                                   */
/*--------------------------------------------------------------------------*/

    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[pd[0]][k] += 0.0625*(v[cd[8]][k] + v[cd[17]][k] + v[cd[18]][k]);
      v[pd[1]][k] += (0.3125*v[cd[8]][k]
		      + 0.0625*(-v[cd[17]][k] - v[cd[18]][k]));
      v[pd[4]][k] += (-0.3125*v[cd[8]][k]
		      + 0.1875*(-v[cd[17]][k] - v[cd[18]][k]));
      v[pd[5]][k] += (v[cd[9]][k] + 0.9375*v[cd[8]][k]
		      + 0.1875*(v[cd[17]][k] + v[cd[18]][k]));
      v[pd[6]][k] += -0.125*v[cd[18]][k];
      v[pd[8]][k] += -0.125*v[cd[17]][k];
      v[pd[10]][k] += 0.375*v[cd[18]][k];
      v[pd[12]][k] += 0.375*v[cd[17]][k];
      v[pd[18]][k] += 0.75*v[cd[17]][k];
      v[pd[19]][k] += 0.75*v[cd[18]][k];
    }
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  node0 = drdv->fe_space->admin->mesh->node[FACE];
  n0 = admin->n0_dof[FACE];

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;

    get_dof_indices3_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices3_3d(cd, el->child[0], admin, bas_fcts);
    cdi = cd[16];

    switch(lr_set)
    {
    case 1:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[0]][k] += 0.0625*(v[cd[12]][k] + v[cdi][k] - v[cd[18]][k]);
	v[pd[1]][k] += 0.0625*(v[cd[12]][k] + v[cdi][k] + v[cd[18]][k]);
	v[pd[4]][k] += (0.0625*(-v[cd[12]][k] - v[cdi][k])
			+ 0.1875*v[cd[18]][k]);
	v[pd[5]][k] += (0.0625*(-v[cd[12]][k] - v[cdi][k])
			- 0.1875*v[cd[18]][k]);
	v[pd[6]][k] += (-0.25*v[cd[12]][k] - 0.125*v[cdi][k]
			+ 0.375*v[cd[18]][k]);
	v[pd[7]][k] += 0.5*v[cd[12]][k];
	v[pd[8]][k] += -0.125*v[cdi][k];
	v[pd[10]][k] += -0.25*v[cd[12]][k] + 0.125*(-v[cdi][k]-v[cd[18]][k]);
	v[pd[11]][k] += 0.5*v[cd[12]][k];
	v[pd[12]][k] += -0.125*v[cdi][k];
	v[pd[16]][k] += 0.5*v[cdi][k];
	v[pd[17]][k] += 0.5*v[cdi][k];
	v[pd[18]][k] += 0.25*v[cdi][k];
	v[pd[19]][k] = (v[cd[13]][k] + 0.5*v[cd[12]][k] + 0.25*v[cdi][k]
			+ 0.75*v[cd[18]][k]);
      }
      break;
    case 2:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[0]][k] += 0.0625*(v[cd[14]][k] + v[cdi][k] - v[cd[17]][k]);
	v[pd[1]][k] += 0.0625*(v[cd[14]][k] + v[cdi][k] + v[cd[17]][k]);
	v[pd[4]][k] += (0.0625*(-v[cd[14]][k] - v[cdi][k])
			+ 0.1875*v[cd[17]][k]);
	v[pd[5]][k] += (0.0625*(-v[cd[14]][k] - v[cdi][k])
			- 0.1875*v[cd[17]][k]);
	v[pd[6]][k] += -0.125*v[cdi][k];
	v[pd[8]][k] += (-0.25*v[cd[14]][k] - 0.125*v[cdi][k]
			+ 0.375*v[cd[17]][k]);
	v[pd[9]][k] += 0.5*v[cd[14]][k];
	v[pd[10]][k] += -0.125*v[cdi][k];
	v[pd[12]][k] += -0.25*v[cd[14]][k] + 0.125*(-v[cdi][k]-v[cd[17]][k]);
	v[pd[13]][k] += 0.5*v[cd[14]][k];
	v[pd[16]][k] += 0.5*v[cdi][k];
	v[pd[17]][k] += 0.5*v[cdi][k];
	v[pd[18]][k] = (v[cd[15]][k] + 0.5*v[cd[14]][k]
			+ 0.25*v[cdi][k] + 0.75*v[cd[17]][k]);
	v[pd[19]][k] += 0.25*v[cdi][k];
      }
      break;
    case 3:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[0]][k] += 0.0625*v[cdi][k];
	v[pd[1]][k] += 0.0625*v[cdi][k];
	v[pd[4]][k] += -0.0625*v[cdi][k];
	v[pd[5]][k] += -0.0625*v[cdi][k];
	v[pd[6]][k] += -0.125*v[cdi][k];
	v[pd[8]][k] += -0.125*v[cdi][k];
	v[pd[10]][k] += -0.125*v[cdi][k];
	v[pd[12]][k] += -0.125*v[cdi][k];
	v[pd[16]][k] += 0.5*v[cdi][k];
	v[pd[17]][k] += 0.5*v[cdi][k];
	v[pd[18]][k] += 0.25*v[cdi][k];
	v[pd[19]][k] += 0.25*v[cdi][k];
      }
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices3_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	cdi = el->child[1]->dof[node0+1][n0];
	DEBUG_TEST_EXIT(cdi == cd[17], "cdi != cd[17]\n");
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += 0.0625*v[cdi][k];
	  v[pd[1]][k] += -0.0625*v[cdi][k];
	  v[pd[4]][k] += -0.1875*v[cdi][k];
	  v[pd[5]][k] += 0.1875*v[cdi][k];
	  v[pd[6]][k] += -0.125*v[cdi][k];
	  v[pd[10]][k] += 0.375*v[cdi][k];
	  v[pd[19]][k] += 0.75*v[cdi][k];
	}
	break;
      case 2:
	cdi = el->child[1]->dof[node0+2][n0];
	DEBUG_TEST_EXIT(cdi == cd[18], "cdi != cd[18]\n");
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += 0.0625*v[cdi][k];
	  v[pd[1]][k] += -0.0625*v[cdi][k];
	  v[pd[4]][k] += -0.1875*v[cdi][k];
	  v[pd[5]][k] += 0.1875*v[cdi][k];
	  v[pd[8]][k] += -0.125*v[cdi][k];
	  v[pd[12]][k] += 0.375*v[cdi][k];
	  v[pd[18]][k] += 0.75*v[cdi][k];
	}
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	cdi = el->child[1]->dof[node0+2][n0];
	DEBUG_TEST_EXIT(cdi == cd[18], "cdi != cd[18]\n");
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += 0.0625*v[cdi][k];
	  v[pd[1]][k] += -0.0625*v[cdi][k];
	  v[pd[4]][k] += -0.1875*v[cdi][k];
	  v[pd[5]][k] += 0.1875*v[cdi][k];
	  v[pd[6]][k] += -0.125*v[cdi][k];
	  v[pd[10]][k] += 0.375*v[cdi][k];
	  v[pd[19]][k] += 0.75*v[cdi][k];
	}
	break;
      case 2:
	cdi = el->child[1]->dof[node0+1][n0];
	DEBUG_TEST_EXIT(cdi == cd[17], "cdi != cd[17]\n");
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += 0.0625*v[cdi][k];
	  v[pd[1]][k] += -0.0625*v[cdi][k];
	  v[pd[4]][k] += -0.1875*v[cdi][k];
	  v[pd[5]][k] += 0.1875*v[cdi][k];
	  v[pd[8]][k] += -0.125*v[cdi][k];
	  v[pd[12]][k] += 0.375*v[cdi][k];
	  v[pd[18]][k] += 0.75*v[cdi][k];
	}
	break;
      }
    }
  }
  return;
}

static const BAS_FCT phi3_3d[N_BAS_LAG_3_3D] ={
  phi3v0_3d, phi3v1_3d,phi3v2_3d, phi3v3_3d,
  phi3e0_3d, phi3e1_3d,phi3e2_3d, phi3e3_3d,
  phi3e4_3d, phi3e5_3d,phi3e6_3d, phi3e7_3d,
  phi3e8_3d,phi3e9_3d,phi3e10_3d,phi3e11_3d,
  phi3c0_3d,phi3c1_3d,phi3c2_3d,phi3c3_3d
};

static const GRD_BAS_FCT grd_phi3_3d[N_BAS_LAG_3_3D] = {
  grd_phi3v0_3d,  grd_phi3v1_3d,
  grd_phi3v2_3d,  grd_phi3v3_3d,
  grd_phi3e0_3d,  grd_phi3e1_3d,
  grd_phi3e2_3d,  grd_phi3e3_3d,
  grd_phi3e4_3d,  grd_phi3e5_3d,
  grd_phi3e6_3d,  grd_phi3e7_3d,
  grd_phi3e8_3d,  grd_phi3e9_3d,
  grd_phi3e10_3d, grd_phi3e11_3d,
  grd_phi3c0_3d,  grd_phi3c1_3d,
  grd_phi3c2_3d,  grd_phi3c3_3d
};

static const D2_BAS_FCT   D2_phi3_3d[N_BAS_LAG_3_3D]  = {
  D2_phi3v0_3d,  D2_phi3v1_3d,
  D2_phi3v2_3d,  D2_phi3v3_3d,
  D2_phi3e0_3d,  D2_phi3e1_3d,
  D2_phi3e2_3d,  D2_phi3e3_3d,
  D2_phi3e4_3d,  D2_phi3e5_3d,
  D2_phi3e6_3d,  D2_phi3e7_3d,
  D2_phi3e8_3d,  D2_phi3e9_3d,
  D2_phi3e10_3d, D2_phi3e11_3d,
  D2_phi3c0_3d,  D2_phi3c1_3d,
  D2_phi3c2_3d,  D2_phi3c3_3d
};

const D3_BAS_FCT D3_phi3_3d[N_BAS_LAG_3_3D]  = {
  D3_phi3v0_3d,  D3_phi3v1_3d,
  D3_phi3v2_3d,  D3_phi3v3_3d,
  D3_phi3e0_3d,  D3_phi3e1_3d,
  D3_phi3e2_3d,  D3_phi3e3_3d,
  D3_phi3e4_3d,  D3_phi3e5_3d,
  D3_phi3e6_3d,  D3_phi3e7_3d,
  D3_phi3e8_3d,  D3_phi3e9_3d,
  D3_phi3e10_3d, D3_phi3e11_3d,
  D3_phi3c0_3d,  D3_phi3c1_3d,
  D3_phi3c2_3d,  D3_phi3c3_3d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element. See also submesh.c.
 *
 * Meaning of the dimensions is the same as in the BAS_FCTS structure
 *
 * [type][orientation][wall][bas_fcts]
 */
static const int trace_mapping_lag_3_3d[2][2][N_WALLS_3D][N_BAS_LAG_3_2D] = {
  { 
    { { 3,  1,  2, 10, 11, 14, 15, 13, 12, 16},
      { 2,  0,  3,  8,  9, 15, 14,  7,  6, 17},
      { 0,  1,  3, 12, 13,  9,  8,  4,  5, 18},
      { 1,  0,  2,  6,  7, 11, 10,  5,  4, 19} }, /* t = 0, o = + */
    { { 1,  3,  2, 15, 14, 11, 10, 12, 13, 16},
      { 0,  2,  3, 14, 15,  9,  8,  6,  7, 17},
      { 1,  0,  3,  8,  9, 13, 12,  5,  4, 18},
      { 0,  1,  2, 10, 11,  7,  6,  4,  5, 19} }  /* t = 0, o = - */
  },
  {
    { { 1,  2,  3, 14, 15, 13, 12, 10, 11, 16},
      { 2,  0,  3,  8,  9, 15, 14,  7,  6, 17},
      { 0,  1,  3, 12, 13,  9,  8,  4,  5, 18},
      { 1,  0,  2,  6,  7, 11, 10,  5,  4, 19} }, /* t = 1, o = + */
    { { 2,  1,  3, 12, 13, 15, 14, 11, 10, 16},
      { 0,  2,  3, 14, 15,  9,  8,  6,  7, 17},
      { 1,  0,  3,  8,  9, 13, 12,  5,  4, 18},
      { 0,  1,  2, 10, 11,  7,  6,  4,  5, 19} }  /* t = 1, o = - */
  }
};

static const BAS_FCTS lagrange3_3d = {
  "lagrange3_3d", 3, 1, N_BAS_LAG_3_3D, N_BAS_LAG_3_3D, 3,
  {1, 0, 2, 1},/* VERTEX,CENTER,EDGE,FACE */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange3_3d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi3_3d, grd_phi3_3d, D2_phi3_3d, D3_phi3_3d,
  NULL, /* fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange3_2d, /* trace space */
  { { { trace_mapping_lag_3_3d[0][0][0], /* t = 0, o = + */
	trace_mapping_lag_3_3d[0][0][1],
	trace_mapping_lag_3_3d[0][0][2],
	trace_mapping_lag_3_3d[0][0][3] },
      { trace_mapping_lag_3_3d[0][1][0], /* t = 0, o = - */
	trace_mapping_lag_3_3d[0][1][1],
	trace_mapping_lag_3_3d[0][1][2],
	trace_mapping_lag_3_3d[0][1][3] } },
    { { trace_mapping_lag_3_3d[1][0][0], /* t = 1, o = + */
	trace_mapping_lag_3_3d[1][0][1],
	trace_mapping_lag_3_3d[1][0][2],
	trace_mapping_lag_3_3d[1][0][3] },
      { trace_mapping_lag_3_3d[1][1][0], /* t = 1, o = - */
	trace_mapping_lag_3_3d[1][1][1],
	trace_mapping_lag_3_3d[1][1][2],
	trace_mapping_lag_3_3d[1][1][3] } } }, /* trace mapping */
  { N_BAS_LAG_3_2D,
    N_BAS_LAG_3_2D,
    N_BAS_LAG_3_2D,
    N_BAS_LAG_3_2D }, /* n_trace_bas_fcts */
  get_dof_indices3_3d,
  get_bound3_3d,
  interpol3_3d,
  interpol_d_3_3d,
  interpol_dow_3_3d,
  get_int_vec3_3d,
  get_real_vec3_3d,
  get_real_d_vec3_3d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec3_3d,
  get_uchar_vec3_3d,
  get_schar_vec3_3d,
  get_ptr_vec3_3d,
  get_real_dd_vec3_3d,
  real_refine_inter3_3d,
  real_coarse_inter3_3d,
  real_coarse_restr3_3d,
  real_d_refine_inter3_3d,
  real_d_coarse_inter3_3d,
  real_d_coarse_restr3_3d,
  (REF_INTER_FCT_D)real_d_refine_inter3_3d,
  (REF_INTER_FCT_D)real_d_coarse_inter3_3d,
  (REF_INTER_FCT_D)real_d_coarse_restr3_3d,
  (void *)&lag_3_3d_data
};
