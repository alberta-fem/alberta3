/*--------------------------------------------------------------------*/
/*--- ALBERTA:  an Adaptive multi Level finite element toolbox using -*/
/*---           Bisectioning refinement and Error control by Residual */
/*---           Techniques for scientific Applications             ---*/
/*---                                                              ---*/
/*--- file: lagrange_4_1d.c                                        ---*/
/*---                                                              ---*/
/*--- description: implementation of the basis functions           ---*/
/*---              lagrange4 in 1d                                 ---*/
/*---                                                              ---*/
/*--- created by: kgs on host enigma                               ---*/
/*---           at 16:55 on 28 of March 2003                       ---*/
/*--------------------------------------------------------------------*/
/*---                                                              ---*/
/*--- authors:   Alfred Schmidt                                    ---*/
/*---            Zentrum fuer Technomathematik                     ---*/
/*---            Fachbereich 3 Mathematik/Informatik               ---*/
/*---            Universitaet Bremen                               ---*/
/*---            Bibliothekstr. 2                                  ---*/
/*---            D-28359 Bremen, Germany                           ---*/
/*---                                                              ---*/
/*---            Kunibert G. Siebert                               ---*/
/*---            Institut fuer Mathematik                          ---*/
/*---            Universitaet Augsburg                             ---*/
/*---            Universitaetsstr. 14                              ---*/
/*---            D-86159 Augsburg, Germany                         ---*/
/*---                                                              ---*/
/*--- http://www.mathematik.uni-freiburg.de/IAM/ALBERTA            ---*/
/*---                                                              ---*/
/*--- (c) by A. Schmidt and K.G. Siebert (1996-2003)               ---*/
/*---                                                              ---*/
/*--------------------------------------------------------------------*/

static const REAL_B bary4_1d[N_BAS_LAG_4_1D] = {
  INIT_BARY_1D(1.0,  0.0),
  INIT_BARY_1D(0.0,  1.0),
  INIT_BARY_1D(0.75, 0.25),
  INIT_BARY_1D(0.5,  0.5),
  INIT_BARY_1D(0.25, 0.75)
};

static LAGRANGE_DATA lag_4_1d_data = {
  bary4_1d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*---  basisfunction 0 located at vertex 0                               ---*/
/*--------------------------------------------------------------------------*/

static REAL phi4_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return
    (((32.0*lambda[0] - 48.0)*lambda[0] + 22.0)*lambda[0] - 3.0)*lambda[0]/3.0;
}

static const REAL *grd_phi4_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128.0*lambda[0] - 144.0)*lambda[0] + 44.0)*lambda[0]/3.0 - 1.0;
  return (const REAL *)grd;
}

static const REAL_B *D2_phi4_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][0] = (128.0*lambda[0] - 96.0)*lambda[0] + 44.0/3.0;
  return (const REAL_B *)D2;
}

static const REAL_BB *
D3_phi4_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BBB D3;
  D3[0][0][0] = 256.0*lambda[0] - 96.0;
  return (const REAL_BB *)D3;
}

static const REAL_BBB *
D4_phi4_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BBBB D4;
  D4[0][0][0][0] = 256.0;
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 1, located at vertex 1                                 */
/*--------------------------------------------------------------------------*/

static REAL phi4_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return
    (((32.0*lambda[1] - 48.0)*lambda[1] + 22.0)*lambda[1] - 3.0)*lambda[1]/3.0;
}

static const REAL *grd_phi4_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = ((128.0*lambda[1] - 144.0)*lambda[1] + 44.0)*lambda[1]/3.0 - 1.0;
  return (const REAL *)grd;
}

static const REAL_B *D2_phi4_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = (128.0*lambda[1] - 96.0)*lambda[1] + 44.0/3.0;
  return (const REAL_B *)D2;
}

static const REAL_BB *D3_phi4_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BBB D3;
  D3[1][1][1] = 256.0*lambda[1] - 96.0;
  return (const REAL_BB *)D3;
}

static const REAL_BBB *
D4_phi4_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BBBB D4;
  D4[1][1][1][1] = 256.0;
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 2, first at the center                              ---*/
/*--------------------------------------------------------------------------*/

static REAL phi4_2_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return ((128.0*lambda[0] - 96.0)*lambda[0] + 16.0)*lambda[0]*lambda[1]/3.0;
}

static const REAL *grd_phi4_2_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128*lambda[0] - 64.0)*lambda[0] + 16.0/3.0)*lambda[1];
  grd[1] = ((128*lambda[0] - 96.0)*lambda[0] + 16.0)*lambda[0]/3.0;
  return (const REAL *)grd;
}

static const REAL_B *D2_phi4_2_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][0] = (256.0*lambda[0] - 64.0)*lambda[1];
  D2[0][1] = D2[1][0] = (128.0*lambda[0] - 64.0)*lambda[0] + 16.0/3.0;
  return (const REAL_B *)D2;
}

static const REAL_BB *D3_phi4_2_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BBB D3;
  D3[0][0][0] = 256.0*lambda[1];
  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 256.0*lambda[0] - 64.0;
  return (const REAL_BB *)D3;
}

static const REAL_BBB *
D4_phi4_2_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BBBB D4;
  D4[0][0][0][1] = D4[0][0][1][0] = D4[0][1][0][0] = D4[1][0][0][0] = 256.0;
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 3, second at the center                             ---*/
/*--------------------------------------------------------------------------*/

static REAL phi4_3_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (4.0*lambda[0] - 1.0)*lambda[0]*(4.0*lambda[1] - 1.0)*lambda[1]*4.0;
}

static const REAL *grd_phi4_3_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 4.0*(8.0*lambda[0] - 1.0)*lambda[1]*(4.0*lambda[1] - 1.0);
  grd[1] = 4.0*lambda[0]*(4.0*lambda[0] - 1.0)*(8.0*lambda[1] - 1.0);
  return (const REAL *)grd;
}

static const REAL_B *D2_phi4_3_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][0] = 32.0*lambda[1]*(4.0*lambda[1] - 1.0);
  D2[0][1] = D2[1][0] = 4.0*(8.0*lambda[0] - 1.0)*(8.0*lambda[1] - 1.0);
  D2[1][1] = 32.0*lambda[0]*(4.0*lambda[0] - 1.0);
  return (const REAL_B *)D2;
}

static const REAL_BB *D3_phi4_3_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BBB D3;
  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 256.0*lambda[1] - 32.0;
  D3[1][1][0] = D3[1][0][1] = D3[0][1][1] = 256.0*lambda[0] - 32.0;
  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4_3_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BBBB D4;
  D4[0][0][1][1] = D4[1][1][0][0] = 
    D4[0][1][0][1] = D4[1][0][1][0] =
    D4[1][0][0][1] = D4[0][1][1][0] = 256.0;
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 4, third at the center                              ---*/
/*--------------------------------------------------------------------------*/

static REAL phi4_4_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return((128.0*lambda[1] - 96.0)*lambda[1] + 16.0)*lambda[1]*lambda[0]/3.0;
}

static const REAL *grd_phi4_4_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128*lambda[1] - 96.0)*lambda[1] + 16.0)*lambda[1]/3.0;
  grd[1] = ((128*lambda[1] - 64.0)*lambda[1] + 16.0/3.0)*lambda[0];
  return (const REAL *)grd;
}

static const REAL_B *D2_phi4_4_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[1][1] = (256.0*lambda[1] - 64.0)*lambda[0];
  D2[0][1] = D2[1][0] = (128.0*lambda[1] - 64.0)*lambda[1] + 16.0/3.0;
  return (const REAL_B *)D2;
}

static const REAL_BB *D3_phi4_4_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;
  D3[1][1][1] = 256.0*lambda[0];
  D3[1][1][0] = D3[1][0][1] = D3[0][1][1] = 256.0*lambda[1] - 64.0;
  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4_4_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;
  D4[1][1][1][0] = D4[1][1][0][1] = D4[1][0][1][1] = D4[0][1][1][1] = 256.0;
  return (const REAL_BBB *)D4;
}

/******************************************************************************/

#undef DEF_EL_VEC_4_1D
#define DEF_EL_VEC_4_1D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_4_1D, N_BAS_LAG_4_1D)

#undef DEFUN_GET_EL_VEC_4_1D
#define DEFUN_GET_EL_VEC_4_1D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  get_##name##4_1d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("get_"#name"4_1d");					\
    static DEF_EL_VEC_4_1D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, node, inode, ibas;						\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    /*--------------------------------------------------------------------*/ \
    /*--- DOFs at vertices                                             ---*/ \
    /*--------------------------------------------------------------------*/ \
									\
    node = (admin)->mesh->node[VERTEX];					\
    n0   = (admin)->n0_dof[VERTEX];					\
    for (ibas = inode = 0; inode < N_VERTICES_1D; inode++, ibas++) {	\
      dof = dofptr[node+inode][n0];					\
      body;								\
    }									\
									\
    /*--------------------------------------------------------------------*/ \
    /*--- DOFs at center                                               ---*/ \
    /*--------------------------------------------------------------------*/ \
									\
    node = (admin)->mesh->node[CENTER];					\
    n0   = (admin)->n0_dof[CENTER];					\
    for (inode = 0; inode < 3; inode++, ibas++) {			\
      dof = dofptr[node][n0+inode];					\
      body;								\
    }									\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_4_1D
#define DEFUN_GET_EL_DOF_VEC_4_1D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_4_1D(_##name##_vec, type, dv->fe_space->admin,	\
			ASSIGN(dv->vec[dof], rvec[ibas]),		\
			const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  get_##name##_vec4_1d(type##_VEC_TYPE *vec, const EL *el,		\
		       const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
									\
    if (vec != NULL || vec_loc == NULL) {				\
      return get__##name##_vec4_1d(vec, el, dv);			\
    } else {								\
      get__##name##_vec4_1d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy

#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/*--------------------------------------------------------------------*/
/*--- function for accessing local DOFs on an element              ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_4_1D(dof_indices, DOF, admin,
		      rvec[ibas] = dof,
		      const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

/*--------------------------------------------------------------------*/
/*--- function for accessing boundary type of DOFs                 ---*/
/*--------------------------------------------------------------------*/

static const EL_BNDRY_VEC *get_bound4_1d(BNDRY_FLAGS *vec,
					 const EL_INFO *el_info,
					 const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound4_1d");
  static DEF_EL_VEC_4_1D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int ibas = 0, i, j;

  DEBUG_TEST_FLAG(FILL_BOUND, el_info);

/*--------------------------------------------------------------------*/
/*--- basis functions at vertices                                  ---*/
/*--------------------------------------------------------------------*/

  for (i = 0; i < N_VERTICES_1D; i++) {
    BNDRY_FLAGS_CPY(rvec[i], el_info->vertex_bound[i]);
  }

/*--------------------------------------------------------------------*/
/*--- basis functions at center                                    ---*/
/*--------------------------------------------------------------------*/

  for (j = 0; j < 3; j++) {
    BNDRY_FLAGS_INIT(rvec[ibas]);
  }

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL(/**/, 4, 1, N_BAS_LAG_4_1D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL_D(/**/, 4, 1, N_BAS_LAG_4_1D);

GENERATE_INTERPOL_DOW(/**/, 4, 1, N_BAS_LAG_4_1D);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_1D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_1D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_1D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_1D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_1D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_1D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_1D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for interpolaton DOF_REAL_VECs during refinement    ---*/
/*--------------------------------------------------------------------*/

static void refine_inter4_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL pvec[N_BAS_LAG_4_1D];
  DOF cdof[N_BAS_LAG_4_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  el = list->el_info.el;
  get_real_vec4_1d(pvec, el, dv);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, first child                           ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices4_1d(cdof, el->child[0], admin, bas_fcts);

  v[cdof[1]] = (pvec[3]);
  v[cdof[2]] = (0.2734375*pvec[0] - 0.0390625*pvec[1] + 1.09375*pvec[2]
		- 0.546875*pvec[3] + 0.21875*pvec[4]);
  v[cdof[3]] = (pvec[2]);
  v[cdof[4]] = (-0.0390625*pvec[0] + 0.0234375*pvec[1] + 0.46875*pvec[2]
		+ 0.703125*pvec[3] - 0.15625*pvec[4]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices4_1d(cdof, el->child[1], admin, bas_fcts);

  v[cdof[2]] = (0.0234375*pvec[0] - 0.0390625*pvec[1] - 0.15625*pvec[2]
		+ 0.703125*pvec[3] + 0.46875*pvec[4]);
  v[cdof[3]] = (pvec[4]);
  v[cdof[4]] = (-0.0390625*pvec[0] + 0.2734375*pvec[1] + 0.21875*pvec[2]
		- 0.546875*pvec[3] + 1.09375*pvec[4]);

  return;
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton DOF_REAL_D_VECs during refinement  ---*/
/*--------------------------------------------------------------------*/

static void refine_inter_d4_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL_D pvec[N_BAS_LAG_4_1D];
  DOF cdof[N_BAS_LAG_4_1D];
  EL              *el;
  int             n;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL_D          *v = dv->vec;

  el = list->el_info.el;
  get_real_d_vec4_1d(pvec, el, dv);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, first child                           ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices4_1d(cdof, el->child[0], admin, bas_fcts);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[cdof[1]][n] = (pvec[3][n]);
    v[cdof[2]][n] = (0.2734375*pvec[0][n] - 0.0390625*pvec[1][n]
		     + 1.09375*pvec[2][n] - 0.546875*pvec[3][n]
		     + 0.21875*pvec[4][n]);
    v[cdof[3]][n] = (pvec[2][n]);
    v[cdof[4]][n] = (-0.0390625*pvec[0][n] + 0.0234375*pvec[1][n]
		     + 0.46875*pvec[2][n] + 0.703125*pvec[3][n]
		     - 0.15625*pvec[4][n]);
  }

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices4_1d(cdof, el->child[1], admin, bas_fcts);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[cdof[2]][n] = (0.0234375*pvec[0][n] - 0.0390625*pvec[1][n]
		     - 0.15625*pvec[2][n] + 0.703125*pvec[3][n]
		     + 0.46875*pvec[4][n]);
    v[cdof[3]][n] = (pvec[4][n]);
    v[cdof[4]][n] = (-0.0390625*pvec[0][n] + 0.2734375*pvec[1][n]
		     + 0.21875*pvec[2][n] - 0.546875*pvec[3][n]
		     + 1.09375*pvec[4][n]);
  }
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton DOF_REAL_VECs during coarsening    ---*/
/*--------------------------------------------------------------------*/

static void coarse_inter4_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL cvec[N_BAS_LAG_4_1D];
  DOF pdof[N_BAS_LAG_4_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices4_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_vec4_1d(cvec, el->child[0], dv);

  v[pdof[2]] = (cvec[3]);
  v[pdof[3]] = (cvec[1]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_real_vec4_1d(cvec, el->child[1], dv);

  v[pdof[4]] = (cvec[3]);

  return;
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton DOF_REAL_D_VECs during coarsening  ---*/
/*--------------------------------------------------------------------*/

static void coarse_inter_d4_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL_D cvec[N_BAS_LAG_4_1D];
  DOF pdof[N_BAS_LAG_4_1D];
  EL              *el;
  int             n;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL_D          *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices4_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec4_1d(cvec, el->child[0], dv);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[pdof[2]][n] = (cvec[3][n]);
    v[pdof[3]][n] = (cvec[1][n]);
  }

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec4_1d(cvec, el->child[1], dv);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[pdof[4]][n] = (cvec[3][n]);
  }

  return;
}

/*--------------------------------------------------------------------*/
/*--- function for restriction of DOF_REAL_VECs during coarsening  ---*/
/*--------------------------------------------------------------------*/

static void coarse_restr4_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL cvec[N_BAS_LAG_4_1D];
  DOF pdof[N_BAS_LAG_4_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices4_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_vec4_1d(cvec, el->child[0], dv);

  v[pdof[0]] += (0.2734375*cvec[2] - 0.0390625*cvec[4]);
  v[pdof[1]] += (-0.0390625*cvec[2] + 0.0234375*cvec[4]);
  v[pdof[2]]  = (1.09375*cvec[2] + cvec[3] + 0.46875*cvec[4]);
  v[pdof[3]]  = (cvec[1] - 0.546875*cvec[2] + 0.703125*cvec[4]);
  v[pdof[4]]  = (0.21875*cvec[2] - 0.15625*cvec[4]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from second child                     ---*/
/*--------------------------------------------------------------------*/

  get_real_vec4_1d(cvec, el->child[1], dv);

  v[pdof[0]] += (0.0234375*cvec[2] - 0.0390625*cvec[4]);
  v[pdof[1]] += (-0.0390625*cvec[2] + 0.2734375*cvec[4]);
  v[pdof[2]] += (-0.15625*cvec[2] + 0.21875*cvec[4]);
  v[pdof[3]] += (0.703125*cvec[2] - 0.546875*cvec[4]);
  v[pdof[4]] += (0.46875*cvec[2] + cvec[3] + 1.09375*cvec[4]);

  return;
}

/*--------------------------------------------------------------------*/
/*--- function for restriction of DOF_REAL_D_VECs during coarsening --*/
/*--------------------------------------------------------------------*/

static void coarse_restr_d4_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL_D cvec[N_BAS_LAG_4_1D];
  DOF pdof[N_BAS_LAG_4_1D];
  EL              *el;
  int             n;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL_D          *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices4_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec4_1d(cvec, el->child[0], dv);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[pdof[0]][n] += (0.2734375*cvec[2][n] - 0.0390625*cvec[4][n]);
    v[pdof[1]][n] += (-0.0390625*cvec[2][n] + 0.0234375*cvec[4][n]);
    v[pdof[2]][n]  = (1.09375*cvec[2][n] + cvec[3][n] + 0.46875*cvec[4][n]);
    v[pdof[3]][n]  = (cvec[1][n] - 0.546875*cvec[2][n] + 0.703125*cvec[4][n]);
    v[pdof[4]][n]  = (0.21875*cvec[2][n] - 0.15625*cvec[4][n]);
  }

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from second child                     ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec4_1d(cvec, el->child[1], dv);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[pdof[0]][n] += (0.0234375*cvec[2][n] - 0.0390625*cvec[4][n]);
    v[pdof[1]][n] += (-0.0390625*cvec[2][n] + 0.2734375*cvec[4][n]);
    v[pdof[2]][n] += (-0.15625*cvec[2][n] + 0.21875*cvec[4][n]);
    v[pdof[3]][n] += (0.703125*cvec[2][n] - 0.546875*cvec[4][n]);
    v[pdof[4]][n] += (0.46875*cvec[2][n] + cvec[3][n] + 1.09375*cvec[4][n]);
  }

  return;
}

/*--------------------------------------------------------------------*/
/*--- Collect all information about basis functions                ---*/
/*--------------------------------------------------------------------*/

static const BAS_FCT phi4_1d[N_BAS_LAG_4_1D] =
{
  phi4_0_1d, phi4_1_1d, phi4_2_1d, phi4_3_1d, phi4_4_1d
};

static const GRD_BAS_FCT grd_phi4_1d[N_BAS_LAG_4_1D] =
{
  grd_phi4_0_1d, grd_phi4_1_1d, grd_phi4_2_1d, grd_phi4_3_1d, grd_phi4_4_1d
};

static const D2_BAS_FCT D2_phi4_1d[N_BAS_LAG_4_1D] =
{
  D2_phi4_0_1d, D2_phi4_1_1d, D2_phi4_2_1d, D2_phi4_3_1d, D2_phi4_4_1d
};

static const D3_BAS_FCT D3_phi4_1d[N_BAS_LAG_4_1D] =
{
  D3_phi4_0_1d, D3_phi4_1_1d, D3_phi4_2_1d, D3_phi4_3_1d, D3_phi4_4_1d
};

static const D4_BAS_FCT D4_phi4_1d[N_BAS_LAG_4_1D] =
{
  D4_phi4_0_1d, D4_phi4_1_1d, D4_phi4_2_1d, D4_phi4_3_1d, D4_phi4_4_1d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element.
 */
static const int trace_mapping_lag_4_1d[N_WALLS_1D][N_BAS_LAG_0D] = {
  { 1 }, { 0 }
};

static const BAS_FCTS lagrange4_1d =
{
  "lagrange4_1d", 1, 1, N_BAS_LAG_4_1D, N_BAS_LAG_4_1D, 4,
  {1, 3, 0, 0},  /* VERTEX, CENTER, EDGE, FACE   */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange4_1d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi4_1d, grd_phi4_1d, D2_phi4_1d, D3_phi4_1d, D4_phi4_1d,
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange_0d,                             /* trace space */
  { { { trace_mapping_lag_4_1d[0],
	trace_mapping_lag_4_1d[1], }, }, }, /* trace mapping */
  { N_BAS_LAG_0D, N_BAS_LAG_0D, },          /* n_trace_bas_fcts */
  get_dof_indices4_1d,
  get_bound4_1d,
  interpol4_1d,
  interpol_d_4_1d,
  interpol_dow_4_1d,
  get_int_vec4_1d,
  get_real_vec4_1d,
  get_real_d_vec4_1d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec4_1d,
  get_uchar_vec4_1d,
  get_schar_vec4_1d,
  get_ptr_vec4_1d,
  get_real_dd_vec4_1d,
  refine_inter4_1d,
  coarse_inter4_1d,
  coarse_restr4_1d,
  refine_inter_d4_1d,
  coarse_inter_d4_1d,
  coarse_restr_d4_1d,
  (REF_INTER_FCT_D)refine_inter_d4_1d,
  (REF_INTER_FCT_D)coarse_inter_d4_1d,
  (REF_INTER_FCT_D)coarse_restr_d4_1d,
  (void *)&lag_4_1d_data
};
