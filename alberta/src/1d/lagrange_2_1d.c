/*--------------------------------------------------------------------*/
/*--- ALBERTA:  an Adaptive multi Level finite element toolbox using -*/
/*---           Bisectioning refinement and Error control by Residual */
/*---           Techniques for scientific Applications             ---*/
/*---                                                              ---*/
/*--- file: langrange_2_1d.c                                       ---*/
/*---                                                              ---*/
/*--- description: implementation of the basis functions           ---*/
/*---              lagrange2 in 1d                                 ---*/
/*---                                                              ---*/
/*--- created by: kgs on host enigma                               ---*/
/*---           at 16:14 on 28 of March 2003                       ---*/
/*--------------------------------------------------------------------*/
/*---                                                              ---*/
/*--- authors:   Alfred Schmidt                                    ---*/
/*---            Zentrum fuer Technomathematik                     ---*/
/*---            Fachbereich 3 Mathematik/Informatik               ---*/
/*---            Universitaet Bremen                               ---*/
/*---            Bibliothekstr. 2                                  ---*/
/*---            D-28359 Bremen, Germany                           ---*/
/*---                                                              ---*/
/*---            Kunibert G. Siebert                               ---*/
/*---            Institut fuer Mathematik                          ---*/
/*---            Universitaet Augsburg                             ---*/
/*---            Universitaetsstr. 14                              ---*/
/*---            D-86159 Augsburg, Germany                         ---*/
/*---                                                              ---*/
/*--- http://www.mathematik.uni-freiburg.de/IAM/ALBERTA            ---*/
/*---                                                              ---*/
/*--- (c) by A. Schmidt and K.G. Siebert (1996-2003)               ---*/
/*---                                                              ---*/
/*--------------------------------------------------------------------*/

/*---8<---------------------------------------------------------------------*/
/*---   second order                                                     ---*/
/*--------------------------------------------------------------------->8---*/

static const REAL_B bary2_1d[N_BAS_LAG_2_1D] = {
  INIT_BARY_1D(1.0, 0.0),
  INIT_BARY_1D(0.0, 1.0),
  INIT_BARY_1D(0.5, 0.5)
};

static LAGRANGE_DATA lag_2_1d_data = {
  bary2_1d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*---  basisfunction 0 located at vertex 0                               ---*/
/*--------------------------------------------------------------------------*/

static REAL phi2_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[0]*(2.0*lambda[0] - 1.0));
}

static const REAL *grd_phi2_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0.0};

  grd[0] = 4.0*lambda[0] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B *D2_phi2_0_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {INIT_BARY_1D(4.0, 0.0),
			     INIT_BARY_1D(0.0, 0.0),};

  return(D2);
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 1 located at vertex 1                               ---*/
/*--------------------------------------------------------------------------*/

static REAL phi2_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[1]*(2.0*lambda[1] - 1.0));
}

static const REAL *grd_phi2_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0.0};
  grd[1] = 4.0*lambda[1] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B *D2_phi2_1_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {INIT_BARY_1D(0.0, 0.0),
			     INIT_BARY_1D(0.0, 4.0),};

  return(D2);
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 2 located at the center                             ---*/
/*--------------------------------------------------------------------------*/

static REAL phi2_2_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[0]*lambda[1]);
}

static const REAL *grd_phi2_2_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0.0};

  grd[0] = 4.0*lambda[1];
  grd[1] = 4.0*lambda[0];
  return((const REAL *) grd);
}

static const REAL_B *D2_phi2_2_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {INIT_BARY_1D(0.0, 4.0),
			     INIT_BARY_1D(4.0, 0.0),};

  return(D2);
}

/******************************************************************************/

#undef DEF_EL_VEC_2_1D
#define DEF_EL_VEC_2_1D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_2_1D, N_BAS_LAG_2_1D)

#undef DEFUN_GET_EL_VEC_2_1D
#define DEFUN_GET_EL_VEC_2_1D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  get_##name##2_1d(type##_VEC_TYPE * vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("get_"#name"2_1d");					\
    static DEF_EL_VEC_2_1D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, node, inode, ibas;						\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    /*--------------------------------------------------------------------*/ \
    /*--- DOFs at vertices                                             ---*/ \
    /*--------------------------------------------------------------------*/ \
									\
    node = (admin)->mesh->node[VERTEX];					\
    n0   = (admin)->n0_dof[VERTEX];					\
    for (ibas = inode = 0; inode < N_VERTICES_1D; inode++, ibas++) {	\
      dof = dofptr[node+inode][n0];					\
      body;								\
    }									\
									\
    /*--------------------------------------------------------------------*/ \
    /*--- DOFs at center                                               ---*/ \
    /*--------------------------------------------------------------------*/ \
									\
    node = (admin)->mesh->node[CENTER];					\
    n0   = (admin)->n0_dof[CENTER];					\
    dof = dofptr[node][n0];						\
    body;								\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_2_1D
#define DEFUN_GET_EL_DOF_VEC_2_1D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_2_1D(_##name##_vec, type, dv->fe_space->admin,	\
			ASSIGN(dv->vec[dof], rvec[ibas]),		\
			const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  get_##name##_vec2_1d(type##_VEC_TYPE *vec, const EL *el,		\
		       const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
									\
    if (vec != NULL || vec_loc == NULL) {				\
      return get__##name##_vec2_1d(vec, el, dv);			\
    } else {								\
      get__##name##_vec2_1d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy

#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/*--------------------------------------------------------------------*/
/*--- function for accessing local DOFs on an element              ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_2_1D(dof_indices, DOF, admin,
		      rvec[ibas] = dof,
		      const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

/*--------------------------------------------------------------------*/
/*--- function for accessing boundary type of DOFs                 ---*/
/*--------------------------------------------------------------------*/

static const EL_BNDRY_VEC *get_bound2_1d(BNDRY_FLAGS *vec,
					 const EL_INFO *el_info,
					 const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound2_1d");
  static DEF_EL_VEC_2_1D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int i;

  DEBUG_TEST_FLAG(FILL_BOUND, el_info);

/*--------------------------------------------------------------------*/
/*--- basis functions at vertices                                  ---*/
/*--------------------------------------------------------------------*/

  for (i = 0; i < N_VERTICES_1D; i++) {
    BNDRY_FLAGS_CPY(rvec[i], el_info->vertex_bound[i]);
  }

/*--------------------------------------------------------------------*/
/*--- basis functions at center                                    ---*/
/*--------------------------------------------------------------------*/

  BNDRY_FLAGS_INIT(rvec[i]);

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL(/**/, 2, 1, N_BAS_LAG_2_1D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL_D(/**/, 2, 1, N_BAS_LAG_2_1D);

GENERATE_INTERPOL_DOW(/**/, 2, 1, N_BAS_LAG_2_1D);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_1D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_1D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_1D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_1D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_1D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_1D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_1D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for interpolaton of DOF_REAL_VECs during refinement ---*/
/*--------------------------------------------------------------------*/

static void refine_inter2_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL pvec[N_BAS_LAG_2_1D];
  DOF cdof[N_BAS_LAG_2_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  el = list->el_info.el;
  get_real_vec2_1d(pvec, el, dv);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, first child                           ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices2_1d(cdof, el->child[0], admin, bas_fcts);

  v[cdof[1]] = (pvec[2]);
  v[cdof[2]] = (0.375*pvec[0] - 0.125*pvec[1] + 0.75*pvec[2]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices2_1d(cdof, el->child[1], admin, bas_fcts);

  dv->vec[cdof[2]] = (-0.125*pvec[0] + 0.375*pvec[1] + 0.75*pvec[2]);
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton of DOF_REAL_D_VECs during refinement -*/
/*--------------------------------------------------------------------*/

static void refine_inter_d2_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL_D pvec[N_BAS_LAG_2_1D];
  DOF cdof[N_BAS_LAG_2_1D];
  EL              *el;
  int             n;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL_D          *v = dv->vec;

  el = list->el_info.el;
  get_real_d_vec2_1d(pvec, el, dv);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, first child                           ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices2_1d(cdof, el->child[0], admin, bas_fcts);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[cdof[1]][n] = (pvec[2][n]);
    v[cdof[2]][n] = (0.375*pvec[0][n] - 0.125*pvec[1][n] + 0.75*pvec[2][n]);
  }

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices2_1d(cdof, el->child[1], admin, bas_fcts);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[cdof[2]][n] = (-0.125*pvec[0][n] + 0.375*pvec[1][n] + 0.75*pvec[2][n]);
  }

  return;
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton of DOF_REAL_VECs during coarsening ---*/
/*--------------------------------------------------------------------*/

static void coarse_inter2_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL cvec[N_BAS_LAG_2_1D];
  DOF pdof[N_BAS_LAG_2_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices2_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_vec2_1d(cvec, el->child[0], dv);

  v[pdof[2]] = (cvec[1]);

#if 0 /* cH wonders: ????? */

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_real_vec2_1d(cvec, el->child[1], dv);

  v[pdof[2]] = (cvec[0]);
#endif
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton of DOF_REAL_D_VECs during coarsening -*/
/*--------------------------------------------------------------------*/

static void coarse_inter_d2_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL_D cvec[N_BAS_LAG_2_1D];
  DOF pdof[N_BAS_LAG_2_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL_D          *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices2_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec2_1d(cvec, el->child[0], dv);

  COPY_DOW(cvec[1], v[pdof[2]]);

#if 0 /* cH wonders: ????? */
/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  cvec = bas_fcts->get_real_d_vec(el->child[1], dv, NULL);

  COPY_DOW(cvec[0], v[pdof[2]]);
#endif
}

/*--------------------------------------------------------------------*/
/*--- function for restriction of DOF_REAL_VECs during coarsening  ---*/
/*--------------------------------------------------------------------*/

static void coarse_restr2_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL cvec[N_BAS_LAG_2_1D];
  DOF pdof[N_BAS_LAG_2_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices2_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_vec2_1d(cvec, el->child[0], dv);

  v[pdof[0]] += (0.375*cvec[2]);
  v[pdof[1]] += (-0.125*cvec[2]);
  v[pdof[2]]  = (cvec[1] + 0.75*cvec[2]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from second child                     ---*/
/*--------------------------------------------------------------------*/

  get_real_vec2_1d(cvec, el->child[1], dv);

  v[pdof[0]] += (-0.125*cvec[2]);
  v[pdof[1]] += (0.375*cvec[2]);
  v[pdof[2]] += (0.75*cvec[2]);
}

/*--------------------------------------------------------------------*/
/*--- function for restriction of DOF_REAL_D_VECs during coarsening --*/
/*--------------------------------------------------------------------*/

static void coarse_restr_d2_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL_D cvec[N_BAS_LAG_2_1D];
  DOF pdof[N_BAS_LAG_2_1D];
  EL              *el;
  int             n;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL_D          *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices2_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec2_1d(cvec, el->child[0], dv);

  for (n = 0; n < DIM_OF_WORLD; n++) {
    v[pdof[0]][n] += (0.375*cvec[2][n]);
    v[pdof[1]][n] += (-0.125*cvec[2][n]);
    v[pdof[2]][n]  = (cvec[1][n] + 0.75*cvec[2][n]);
  }

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from second child                     ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec2_1d(cvec, el->child[1], dv);

  for (n = 0; n < DIM_OF_WORLD; n++) {
    v[pdof[0]][n] += (-0.125*cvec[2][n]);
    v[pdof[1]][n] += (0.375*cvec[2][n]);
    v[pdof[2]][n] += (0.75*cvec[2][n]);
  }
}

/*--------------------------------------------------------------------*/
/*--- Collect all information about basis functions                ---*/
/*--------------------------------------------------------------------*/

static const BAS_FCT phi2_1d[N_BAS_LAG_2_1D] =
{
  phi2_0_1d, phi2_1_1d, phi2_2_1d
};

static const GRD_BAS_FCT grd_phi2_1d[N_BAS_LAG_2_1D] =
{
  grd_phi2_0_1d, grd_phi2_1_1d, grd_phi2_2_1d
};
static const D2_BAS_FCT D2_phi2_1d[N_BAS_LAG_2_1D] =
{
  D2_phi2_0_1d,
  D2_phi2_1_1d,
  D2_phi2_2_1d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element.
 */
static const int trace_mapping_lag_2_1d[N_WALLS_1D][N_BAS_LAG_0D] = {
  { 1 }, { 0 }
};

static const BAS_FCTS lagrange2_1d =
{
  "lagrange2_1d", 1, 1, N_BAS_LAG_2_1D, N_BAS_LAG_2_1D, 2,
  {1, 1, 0, 0},  /* VERTEX, CENTER, EDGE, FACE   */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange2_1d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi2_1d, grd_phi2_1d, D2_phi2_1d,
  NULL, NULL, /* third and fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange_0d,                             /* trace space */
  { { { trace_mapping_lag_2_1d[0],
	trace_mapping_lag_2_1d[1], }, }, }, /* trace mapping */
  { N_BAS_LAG_0D, N_BAS_LAG_0D, },          /* n_trace_bas_fcts */
  get_dof_indices2_1d,
  get_bound2_1d,
  interpol2_1d,
  interpol_d_2_1d,
  interpol_dow_2_1d,
  get_int_vec2_1d,
  get_real_vec2_1d,
  get_real_d_vec2_1d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec2_1d,
  get_uchar_vec2_1d,
  get_schar_vec2_1d,
  get_ptr_vec2_1d,
  get_real_dd_vec2_1d,
  refine_inter2_1d,
  coarse_inter2_1d,
  coarse_restr2_1d,
  refine_inter_d2_1d,
  coarse_inter_d2_1d,
  coarse_restr_d2_1d,
  (REF_INTER_FCT_D)refine_inter_d2_1d,
  (REF_INTER_FCT_D)coarse_inter_d2_1d,
  (REF_INTER_FCT_D)coarse_restr_d2_1d,
  (void *)&lag_2_1d_data
};
