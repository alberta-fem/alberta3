/*--------------------------------------------------------------------------*/
/* ALBERTA:   an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques                                                     */
/*                                                                          */
/* file:     nonlin.h                                                       */
/*                                                                          */
/* description:  solver for an nonlinear elliptic model problem             */
/*                                                                          */
/*               -k \Delta u + \sigma u^4 = f  in \Omega                    */
/*                                      u = g  on \partial \Omega           */
/*                                                                          */
/*               header file with definition of data structures and         */
/*               function definitions                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                        */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

#include <alberta/alberta.h>

/*--------------------------------------------------------------------------*/
/*  structure for collecting data of the problem:                           */
/*    k:     consant heat conductivity                                      */
/*    sigma: Stefan--Bolzmann constant                                      */
/*    g:     pointer to a function for evaluating boundary values           */
/*    f:     pointer to a function for evaluating the right-hand side       */
/*           (heat source)                                                  */
/*    u0:    if not NULL, pointer to a function for evaluating an initial   */
/*           guess for the discrete solution on the macro triangulation     */
/*    u:     if not NULL, pointer to a function for evaluating the true     */
/*           solution (only for test purpose)                               */
/*    grd_u: if not NULL, pointer to a function for evaluating the gradient */
/*           of the true solution (only for test purpose)                   */
/*--------------------------------------------------------------------------*/

typedef struct prob_data PROB_DATA;
struct prob_data
{
  MACRO_DATA   *data;
  REAL          k, sigma;

  REAL        (*g)(const REAL_D x);
  REAL        (*f)(const REAL_D x);

  REAL        (*u0)(const REAL_D x);

  REAL        (*u)(const REAL_D x);
  const REAL *(*grd_u)(const REAL_D x, REAL_D input);
};

/*--- file nlprob.c --------------------------------------------------------*/
const PROB_DATA *init_problem(void);

/*--- file nlsolve.c -------------------------------------------------------*/
int nlsolve(DOF_REAL_VEC *u0, REAL k, REAL sigma, REAL (*f)(const REAL_D),
	    const BNDRY_FLAGS dirichlet_mask);
