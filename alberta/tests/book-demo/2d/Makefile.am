# define tests for "make check"
check_PROGRAMS = ellipt2d heat2d heat-easy2d nonlin2d \
                 ellipt-isoparam2d \
                 ellipt-sphere2d ellipt-periodic2d ellipt-torus2d \
                  \
                 ellipt-dg2d

TESTS = ellipt2d heat2d \
                 ellipt-isoparam2d \
                 ellipt-sphere2d ellipt-torus2d \
                 ellipt-dg2d
# some tests take too much time, don't run them
# ellipt-periodic2d heat-easy2d nonlin2d

# stokes tests require libltdl
if HAVE_LIBLTDL
  check_PROGRAMS += stokes2d quasi-stokes2d quasi-stokes-slip2d
  TESTS += stokes2d quasi-stokes2d quasi-stokes-slip2d
endif

# failing tests
XFAIL_TESTS = ellipt-isoparam2d ellipt-torus2d ellipt-sphere2d

# includes
AM_CPPFLAGS = -I$(top_srcdir) \
              $(ALBERTA_ALL_INCLUDES) \
              @OBSTACK_CPPFLAGS@ \
              $(EXTRA_INC)

# ellipt2d
ellipt2d_SOURCES = ../Common/ellipt.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt2d_DEPENDENCIES = #
ellipt2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
ellipt2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# nonlin2d
nonlin2d_SOURCES = ../Common/nonlin.c ../Common/nonlin.h\
                 ../Common/nlprob.c ../Common/nlsolve.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
nonlin2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
nonlin2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
nonlin2d_DEPENDENCIES = #
nonlin2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
nonlin2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# heat2d
heat2d_SOURCES = ../Common/heat.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
heat2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
heat2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
heat2d_DEPENDENCIES = #
heat2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
heat2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-isoparam2d
ellipt_isoparam2d_SOURCES = ../Common/ellipt-isoparam.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_isoparam2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_isoparam2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_isoparam2d_DEPENDENCIES = #
ellipt_isoparam2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
ellipt_isoparam2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-periodic2d
ellipt_periodic2d_SOURCES = ../Common/ellipt-periodic.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_periodic2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_periodic2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_periodic2d_DEPENDENCIES = #
ellipt_periodic2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
ellipt_periodic2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# heat-easy2d
heat_easy2d_SOURCES = ../Common/heat-easy.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
heat_easy2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
heat_easy2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
heat_easy2d_DEPENDENCIES = #
heat_easy2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
heat_easy2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-sphere2d
ellipt_sphere2d_SOURCES = ../Common/ellipt-sphere.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_sphere2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_sphere2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_sphere2d_DEPENDENCIES = #
ellipt_sphere2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
ellipt_sphere2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-torus2d
ellipt_torus2d_SOURCES = ../Common/ellipt-torus.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_torus2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_torus2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_torus2d_DEPENDENCIES = #
ellipt_torus2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
ellipt_torus2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# stokes2d
stokes2d_SOURCES = ../Common/stokes.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
stokes2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_2d.la\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
stokes2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_2d.la\
 @LIBS@ @DYLOADER_LIBS@
endif
stokes2d_DEPENDENCIES = #
stokes2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
stokes2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# quasi-stokes2d
quasi_stokes2d_SOURCES = ../Common/quasi-stokes.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
quasi_stokes2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_2d.la\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
quasi_stokes2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_2d.la\
 @LIBS@ @DYLOADER_LIBS@
endif
quasi_stokes2d_DEPENDENCIES = #
quasi_stokes2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
quasi_stokes2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# quasi-stokes-slip2d
quasi_stokes_slip2d_SOURCES = ../Common/quasi-stokes-slip.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
quasi_stokes_slip2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_2d.la\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
quasi_stokes_slip2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_2d.la\
 @LIBS@ @DYLOADER_LIBS@
endif
quasi_stokes_slip2d_DEPENDENCIES = #
quasi_stokes_slip2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
quasi_stokes_slip2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-dg2d
ellipt_dg2d_SOURCES = ../Common/ellipt-dg.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_dg2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_dg2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_dg2d_DEPENDENCIES = #
ellipt_dg2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
ellipt_dg2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)
