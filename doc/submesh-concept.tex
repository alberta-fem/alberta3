\section{Submeshes}%
\label{S:Submeshes}%
\label{S:tracemeshes}%
\idx{submeshes!concepts}

\def\vg{\vec{g}}

Probably the most significant new feature introduced in \ALBERTA 2.0
are the automatic generation and maintenance of \emph{submeshes} or
\emph{slave meshes}. We motivate the idea by extending the model
problem \mathref{E:strong} to include inhomogeneous Neumann boundary
conditions:
\begin{subequations}\label{E:strong_neumann}
\begin{alignat}{2}
  -\nabla \cdot A \nabla u + b \cdot \nabla u + c\, u 
       &= f \qquad & &\mbox{in } \Omega,\\
     u &= g_D        & &\mbox{on } \GD,\\
\nO \cdot A \nabla u & = g_N &&\mbox{on } \GN,
\end{alignat}
\end{subequations}
with a given function $g_N:\GN\to\R$. Using the notation of 
Section \ref{S:Dis2ndorder} our weak formulation now consists of
finding a solution $u \in X$, such that $u \in g_D+\Xc$ and
\begin{multline}\label{E:weak_neumann}
\int_\Omega (\nabla \varphi(x)) \cdot A(x) \nabla u(x) 
+ \varphi(x)\, b(x) \cdot  \nabla u(x)
+ c(x)\,\varphi(x) \, u(x)   \, dx =\\
\int_\Omega f(x)\, \varphi(x)\, dx + \int_\GN g_N(x)\, \gamma\varphi(x)\, do(x)
\end{multline}
for all $\vphi \in \Xc$. Here we have used $\gamma$ to denote the weak
trace operator from $H^1(\Omega)$ to $H^\half(\GN)$ \todo{Referenz
einfuegen}. The inhomogeneous Neumann boundary condition therefore
leads to an additional term on the right hand side.

Consider again a finite dimensional subspace $X_h \subset X$ with $N =
\dim\ X_h$ based on a triangulation $\tria$ of $\Omega$. Assume that
$\tria$ is constructed in such a way that
\begin{equation}\label{E:conforming_submesh}
  \overline{\GN} = \bigcup_{T\in\triaT} \qquad\text{with}\qquad \triaT = 
  \{ S\cap \GN; S\in\tria\}
\end{equation}
holds. In other words, the triangulation $\tria$ of $\Omega$ induces a 
triangulation $\triaT$ of $\GN$. We define
\begin{equation*}
  Y_h = \Span \{\gamma\varphi_h; \varphi_h\in X_h \}.
\end{equation*}
Let $X_h$ be chosen as the space of standard Lagrange elements of
order $p$ on the triangulation $\tria$ of $\Omega$. Thanks to
\mathref{E:conforming_submesh} we then have that $Y_h$ is exactly
the corresponding space of Lagrange elements of order $p$ on the
triangulation $\triaT$ of $\GN$. The position of the DOFs of $Y_h$ in space 
coincides with corresponding DOFs of $X_h$.

To be precise, let us again define bases
$\left\{\vphi_1,\dots,\vphi_N\right\}$ of $X_h$ and 
$\left\{\psi_1,\dots,\psi_M\right\}$ of $Y_h$. Then there exists a unique 
injective mapping of indices $J$ with the following properties:
\begin{align*}
  J:\{1,\dots, M\} &\to \{1,\dots,N\},\\
  \psi_i &= \gamma\vphi_{J(i)}\qquad\text{for all }i=1,\dots,M.
\end{align*}
The mesh $\triaT$ satisfying the conformity property
\mathref{E:conforming_submesh} is known as a submesh of $\tria$.

Making use of these properties, we can implement the assemblage of the
additional right hand side as follows. Assume that a load vector $\vf$
as defined in \mathref{E:Fright} is already assembled. Define a
temporary quantity $\vg$ associated with $Y_h$ as
\begin{equation}\label{E:Neumann_right_Gamma}
\vg :=
\left[
\begin{matrix}
\ldual{g_N}{\psi_1}{Y_h^* \times Y_h}\\
\vdots\\
\ldual{g_N}{\psi_M}{Y_h^* \times Y_h}
\end{matrix}
\right],
\end{equation}
with 
\begin{equation*}
  \ldual{g_N}{\psi}{Y_h^* \times Y_h} = \int_\GN g_N(x)\,\psi(x)\,do(x).
\end{equation*}
Let $j\in\{1,\dots,N\}$. We have
\begin{equation*}
  \int_\GN g_N(x)\,\gamma\vphi_{j}(x)\,do(x) =
  \int_\GN g_N(x)\,\gamma\vphi_{J(i)}(x)\,do(x) = 
  \int_\GN g_N(x)\, \psi_i(x)\,do(x)
\end{equation*}
if $j=J(i)$ for some $i\in\{1,\dots,N\}$ and 
\begin{equation*}
  \int_\GN g_N(x)\,\gamma\vphi_{j}(x)\,do(x) = 0
\end{equation*}
otherwise. We may therefore complete the assemblage of $\vf$ using the loop
\begin{equation}
  \vf_{J(i)} = \vf_{J(i)} + \vg_i\qquad \text{for }i=1,\dots,M.
\end{equation}

It follows that we may make use of standard \ALBERTA routines for the
assemblage of right hand sides, here applied to the finite element space
$Y_h$ on the submesh $\triaT$. This enables us to create simple and
clear code. Furthermore, we assembled the right hand side contribution
due to the Neumann boundary condition by traversing the smaller
submesh instead of the bulk mesh, which may reduce computational
effort. On the downside, we now need to store the submesh $\triaT$ as
well as the mapping of indices $J$ in memory.

The current version \ALBERTA 2.0 provides an automatic mechanism to
generate submeshes. The user must only provide a function which will
implement the decision about which subsimplices of which macro
elements will belong to the newly created submesh. \ALBERTA takes care 
of providing an index mapping $J$, given that fitting Lagrange elements 
are used on bulk mesh and submesh.

Most important however is the fact that \ALBERTA will automatically refine and 
coarsen bulk mesh and submeshes \emph{simultaneously}, which means that the 
property \mathref{E:conforming_submesh} is preserved throughout an adaptive 
simulation. The user may even define submeshes of submeshes, allowing 
whole hierarchies of dependent meshes, see Figure~\ref{F:submeshes}.

\begin{figure}[htbp]
\includegraphics[width=0.3\textwidth]{EPS/master_3d}\hfill%
\includegraphics[width=0.3\textwidth]{EPS/slave_2d}\hfill%
\includegraphics[width=0.3\textwidth]{EPS/slave_1d}
\caption{Entire hierarchies of submeshes may be defined.}\label{F:submeshes}
\end{figure}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "alberta"
%%% End: 
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "alberta-book"
%%% End: 
