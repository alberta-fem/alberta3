\chapter{Implementation of model problems}%
\label{CH:model}%
\idx{implementation of model problems|(}

In this chapter we describe the implementation of two stationary model
problems (the linear Poisson equation and a nonlinear
reaction-diffusion equation) and of one time dependent model problem
(the heat equation).  Here we give an overview how to set up an
\ALBERTA program for various applications. We do not go into detail
when refering to \ALBERTA data structures and functions. A detailed
description can be found in Chapter~\ref{CH:data.struct}.  We start
with the easy and straight forward implementation of the Poisson
problem to learn about the basics of \ALBERTA. The examples with the
implementation of the nonlinear reaction-diffusion problem and the
time dependent heat equation are more involved and show the tools of
\ALBERTA for attacking more complex problems.  Removing all \LaTeX\
descriptions of functions and variables results in the source code for
the adaptive solvers.  \smallskip During the installation of \ALBERTA
(described in Section~\ref{S:install}) a tar-archive
\bv\begin{verbatim} PREFIX/share/alberta/alberta-VERSION-demo.tar.gz
\end{verbatim}\ev
is installed as well (\code{PREFIX} denoting the installation prefix,
as specified by the \code{--prefix} parameter for the \code{configure}
script). The tar-archive can be extracted at a location where the
respective user has write permissions:
%%
\bv\begin{verbatim}
jane_john_doe@street ~ $ tar -xf PREFIX/share/alberta/alberta-VERSION-demo.tar.gz
jane_john_doe@street ~ $ cd alberta-VERSION-demo
jane_john_doe@street ~/alberta-VERSION-demo $ less README
jane_john_doe@street ~/alberta-VERSION-demo $ cd src/2d
jane_john_doe@street ~/alberta-VERSION-demo/src/2d $ make ellipt
jane_john_doe@street ~/alberta-VERSION-demo/src/2d $ ./ellipt
\end{verbatim}\ev
%%
The archive extracts into a sub-directory having the same name as the
base-name of the tar-archive. The corresponding ready-to-compile
programs can be found in the files \code{ellipt.c}, \code{heat.c}, and
\code{nonlin.c}, \code{nlprob.c}, \code{nlsolve.c} in the subdirectory
\code{alberta2-demo/src/Common/}. Executable programs for different
space dimensions can be generated in the subdirectories
\code{alberta2-demo/src/1d/}, \code{alberta2-demo/src/2d/}, and
\code{alberta2-demo/src/3d/} by calling \code{make ellipt}, \code{make
  nonlin}, and \code{make heat}. There are also a couple of other
programs, please refer to the file \code{README} in the top-level
directory of the demo-package. The idea was to generate one variant of
the \code{ellipt.c} program for each new feature introduced for the
current \ALBERTA version (higher order parametric meshes, higher
co-dimension parametric meshes, periodic meshes, vector-valued basis
functions and direct sums of finite element spaces, limited support
for DG-methods). Mostly, these programs have the name \code{ellipt-FEATURE.c}.

The make-files in the demo-package interprete a \code{DEBUG}-switch
specified on the command-line. This can be useful when modifying the
demo-programs to suite the user's own needs. The resulting programs
will be compiled with debugging information, such that they can be run
from within a source-level debugger. Mind the leading call to
\code{make clean}, the \code{make}-program cannot know that it should
remake the programs!
%%
\bv\begin{verbatim}
jane_john_doe@street ~/alberta-VERSION-demo/src/2d $ make DEBUG=1 clean ellipt
jane_john_doe@street ~/alberta-VERSION-demo/src/2d $ gdb ellipt
\end{verbatim}\ev
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\code{libdemo.a}}
\label{S:libdemo}

The example programs share some common routines for processing
command-line switches, parameter parsing and for some sort of
online-graphics. These routines consequently have been put into a
small library called \code{libdemo.a}. The proto-types for the support
functions are provided through the file \code{alberta-demo.h}, its
essential part looks like follows:
%%
\bv\begin{lstlisting}
#include <limits.h>
#ifndef PATH_MAX
# define PATH_MAX 1024
#endif

#include <alberta.h>
#include "graphics.h"
#include "geomview-graphics.h"

extern void parse_parameters(int argc, char *argv[], const char *init_file);
\end{lstlisting}\ev

\subsection{Online-graphics}
\label{S:online_graphics}
%%
As can be seen in the source-code listing above, the definitions, for
graphical are in turn included from \code{graphics.h} and
\code{geomview-graphics.h}. The demo-programs described in this manual
use only the definitions from \code{graphics.h}, resulting in either a
home-brewed 2d graphics, or output through the \code{gltools} package,
if that could be found during the configuration of the \ALBERTA
distribution.
%%
\bv\begin{lstlisting}
void graphics(MESH *mesh, DOF_REAL_VEC *u_h, REAL (*get_est)(EL *el),
              REAL (*u)(const REAL_D x), REAL time);

void graphics_d(MESH *mesh, DOF_REAL_VEC_D *u_h, DOF_REAL_VEC *p_h,
                REAL (*get_est)(EL *el),
                const REAL *(*u)(const REAL_D val, REAL_D x), REAL time);
\end{lstlisting}\ev
%%
The proto-type for the \code{geomview}-interface looks like follows:
%%
\bv\begin{lstlisting}
extern void togeomview(MESH *mesh,
		       const DOF_REAL_VEC *u_h,
		       REAL uh_min, REAL uh_max,
		       REAL (*get_est)(EL *el),
		       REAL est_min, REAL est_max,
		       REAL (*u_loc)(const EL_INFO *el_info,
				     const REAL_B lambda,
				     void *ud),
		       void *ud, FLAGS fill_flags,
		       REAL u_min, REAL u_max);
\end{lstlisting}\ev
%%
Geomview is used by the demonstration programs for parametric meshes
in higher \mbox{(co-)dimension}. We refer the reader to the example
programs for the calling conventions for the graphic-routines
(although we know that these should be explained in some more detail).
Specifically, when \code{gltools} is in use, then pressing the key
``\code{h}'' in one of the output-windows displays a very brief online
help in the terminal the program is running in.

As \ALBERTA was developed in an environment where mostly Unix-like
operating systems were in use, the online-graphics uses the X window
system (\url{www.xorg.org}), so redirection of graphical output to
other other machines by means of the \code{DISPLAY} environment
variable is possible.

\subsection{\code{parse\_parameters()}}
\label{S:parse_parameters}
We give a more detailed explanation for the following routine:
\begin{description}
\item[Prototype]~\hfill
  \bv\begin{lstlisting}[label=fct:parse_parameters]
  void parse_parameters(int argc, char *argv[], const char *init_file);
\end{lstlisting}\ev
\item[Parameters]~\hfill
  \begin{descr}
  \kitem{argc, argv} The program's command-line parameters, as passed to
    the \code{main()} function. See any \code{C} programming manual.
    %% 
  \kitem{init\_file} The name of the file containing the parameters,
    usually having the form \code{"INIT/<program>.dat"}, but the name
    is arbitrary and the choice is left to the application.
  \end{descr}
\item[Description]~\hfill

  The function \code{parse\_parameters()} initializes the access to
  parameters defined in parameter files, commonly found in
  %% 
  \bv\begin{verbatim}
alberta-VERSION-demo/src/2d/INIT/<program>.dat
\end{verbatim}\ev
  %% 
  and likewise for the other dimensions. The access to the parameters is
  explained in greater detail, especially in the section dealing with
  the demonstration for the Poisson-problem, see \secref{S:poisson-impl}
  below. The actual source-code for \code{parse\_parameters()} is
  contained in \code{src/Common/cmdline.c} (the path being relative to
  the demo-package).

  \code{parse\_parameters()} implements some command-line switches,
  prominently the \code{-h} or \code{--help} switches:
%%
  \bv\begin{verbatim}
jane_john_doe@street ~/alberta-VERSION-demo/src/2d $ ./ellipt --help
Usage: ./ellipt [-h] [-i INITFILE] [-p PARAMETERS]
[--help] [--init-file=INITFILE] [--parameters=PARAMETERS]
jane_john_doe@street ~/alberta-VERSION-demo/src/2d $ ./ellipt -i myparams
 --parameters="degree=3'do_graphics=0"
  \end{verbatim}\ev
%%
  So \code{-i} or \code{--init-file} allows the user to override the
  name of the default parameter-file, and \code{-p} or
  \code{--parameters} allows the user to override specific parameters
  from the parameter-file, in the example above \code{jane\_john\_doe}
  request that the finite element simulation is to be run with
  Lagrange elements of degree $3$ and that no graphical output should
  appear during the simulation. The general format of the argument to
  \code{--parameters} or \code{-p} is
%%
  \bv\begin{verbatim}
KEY1=VALUE1'KEY2=VALUE2...
  \end{verbatim}\ev
%%
  So ``\code{=}'' separates a given key from its value, and a single
  quote separates the key-value pairs.  Note that it might be
  necessary to escape the single quote, or to enclose the entire
  argument by double quotes (as in the example given above).
\end{description}

\input{ellipt}
\input{nonlin}
\input{heat}
\input{files}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "alberta-man"
%%% End: 
