ALBERTA  is an Adaptive multi-Level finite element toolbox using 
         Bisectioning refinement and Error control by Residual
         Techniques for scientific Applications.

-------------------------------------------------------------------------------

Contents
========

I.   Introduction

II.  External Packages (all optional)
         OpenGL
         BLAS
         GPSKCA
         gltools
         GRAPE
         Silo
         OpenDX
         Paraview
         alternate compilers

III. Configure Options
   1.) ALBERTA's installation paths
   2.) Options affecting which versions of ALBERTA are built
   3.) Options controlling specific features
   4.) Options controlling the search-path for external libraries

IV.  Compiler flags (with examples)

V.   Some platform dependent notes and examples

-------------------------------------------------------------------------------


I) Introduction
===============

This is ALBERTA Version 3.1. ALBERTA has a web-page at

        http://www.alberta-fem.de/

and a mailing list

        alberta-fem@math.tu-dortmund.de

The v3 release have a git repository at the following location:

        https://gitlab.com/alberta-fem/alberta3

ALBERTA can be compiled on recent versions of common Linux distributions
and macOS. Other Unices are no longer support. Windows is not directly
supported but WSL2 (Windows subsystem for Linux) should work, MinGW
might work - both are not tested.

Generic installation instructions can be found in the file `./INSTALL'
in this directory. Quick instructions:

./configure --prefix=PREFIX
make
make install

This will configure and compile the package and install the entire
beast in a directory hierarchy below the path-prefix PREFIX, where
PREFIX defaults to `/usr/local/' if the `--prefix' switch is omitted.

You don't need to install ALBERTA, especially if you want to
develop its code. Just use it next to its code.

0) Build system basics
~~~~~~~~~~~~~~~~~~~~~~

ALBERTA uses Autotools as the build system. Autotools consists of
Automake, Autoconf, libtool and uses Make, M4 and various other
pieces of software. It servers ALBERTA as a cross-plattform tool
to detect and configure dependencies, setting system-dependent
variables, to offer a way to set options by the user, and to
generate Makefiles.

Everything defined with the help of Automake, Autoconf, et al. is
put into a huge shell script 'configure'. This script needs to be
executed by the user as the first step. Then the Makefiles can
take over.

ALBERTA's source code is distributed in two ways:
1. As a tar ball including all source code files with pre-computed
   configure script. Using this, Automake and Autoconf are not
   needed, just run the configure script. The tar balls are part
   of every release. Additionally, the build pipeline generates
   the tarball for every commit (and branch respectively) and
   stores it as a build artefact.
2. As a Git clone without any pre-computed file. You have to
   generate Automakefiles and the configure script. To do this,
   Automake and Autoconf and their dependencies are required.
   Run the following two command
    ./generate-alberta-automakefiles.sh
    autoreconf --force --install
   and proceed with running the freshly generated configure script.

1) Dependencies
~~~~~~~~~~~~~~~

To compile ALBERTA from a tarball you need:
- Make
- GCC or Clang
- glibc headers
- tirpc headers (not for macOS)
- pkg-config
- libtool
- gnu-getopt (only macOS)

To generate the configure script, additional dependencies are:
- Automake
- Autoconf

Optional dependencies are needed to provide some features like a
Fortran compiler, BLAS, X11 headers, OpenGL, ParaView, or GRAPE.

2) Tests
~~~~~~~~

Executable are only build as tests and run as tests. 'make check'
builds and executes the tests.


3) The directory layout
~~~~~~~~~~~~~~~~~~~~~~~
Contained in the package-tree are the following sub-directories
(omitting 3rd-level sub-directories below add_ons/):

                                                              ___ 0d/
                        ____________ alberta/ ___ src/ ______|___ 1d/
                       /        \                            |___ 2d/ 
                      /          \__ doc/                    |___ 3d/
                     /                      ___ 1d/          |___ Common/
                    /                      |___ 2d/          |___ alberta*/
                   /_ demo/ __ src/ _______|___ 3d/
                  /                        |___ 4d/  
                 /                         |___ 5d/
                /                          |___ Common/
               /                                     
              /                                          
             /                                
alberta-3.0 /                         
            \___ alberta_util/ ___ src/
             \
              \                ____ bamg2alberta/
               \__ add_ons/___/
                \             \____ block_solve/
                 \             \                                    
                  \             \___ geomview/
                   \             \
                    \             \___ gmv/
                     \             \           
                      \             \___ grape/
                       \             \         
                        \             \___ libalbas/
                         \             \             
                          \             \            
                           \             \___ meshtv/
                            \             \
                             \             \___ paraview/
                              \             \
                               \             \___ static_condensation/
                                \             \
                                 \             \___ triangle2alberta/
                                  \             \
                                   \             \___ write_mesh_fig/
                                    \
                                     \_ configure and other distribution
                                        files

Short description of the sub-directories:

* alberta
  The main package.

* demo 
  The sub-directory `demo/' contains a few demonstration
  programs. After running "make install" (see file `./INSTALL') a
  tar-ball containing all demo programs will be installed in
  `PREFIX/share/alberta/'. The tar-ball can be extracted elsewhere;
  the make-files contained in the tar-ball have appropriate defaults
  for the location of the (installed) ALBERTA libraries and header
  files.

* alberta_util
  This package contains the old ALBERTA util.c file, now split into
  several smaller source files. As of version 2.0 it also contains the
  former SOLVER library. It is compiled to form a separate utility
  library (message macros, ALBERTA memory allocation routines, BLAS
  routines and wrappers, etc.) and should linked to any program using
  the ALBERTA package. It can also be used as a stand-alone utility
  library.

* bamg2alberta
  A converter from the FreeFem++ grid-generatar `bamd' to ALBERTA
  macro file format.

* block_solve
  Block-matrix support.

* geomview
  Simple ALBERTA-OOGL converter.

* gmv
  Stand-alone converter ALBERTA-gmv.

* grape
  GRAPE interface for ALBERTA. Only for non-parametric FE of dimension
  2 and 3.  The GRAPE-interface consists of "make" will create
  "alberta_grapeXX" and alberta_moviXX" where XX is in {22, 33}.
  "make install" will install those programs below `PREFIX/bin/'.

  The GRAPE interface is only installed when the GRAPE library and
  header file are available on your system (determined at configure
  time, use "configure --help" for appropriate command line switches
  for "configure").

* libalbas
  Implementation of some more fancy basis functions (bubbles, direct
  sums).

* meshtv
  MeshTV interface for ALBERTA. Only for non-parametric FE. The MeshTV
  interface consists of the programs "alberta_meshtvX" with X in
  {1,2,3}. These are installed in `PREFIX/bin/' as well.
 
  The interface is only installed if the Silo library and header files
  are available on your system. See "configure --help" for details.

* paraview
  Stand-alone converte ALBERTA-Paraview

* static_condensation
  Condensation of element bubbles, resulting in a drastical reduction
  of the dimension.

* triangle2alberta
  Converter from the Triangle grid generator to the ALBERTA macro-file format.

* write_mesh_fig
  Dump a mesh to disk in a format suitable for xfig(1).

II) External packages
=====================

OpenGL -- you need some implementation of SGI's 3D-API. If your system
	does not have OpenGL you can fetch the free OpenGL
	implementation "MesaGL" from

		http://www.mesa3d.org/

	If you don't have a clue what this is all about then ask your
	system-administrator. In former version OpenGL was required to
        compile ALBERTA, starting with this release graphical output
        is an optional feature.

BLAS -- "Basic Linear Algebra Subprograms"
	You definitely need some version of the BLAS. Often the BLAS
	can be found at "/usr/lib/libblas.a", but this need not be the
	case.

	You should have a look at
	
	http://www.netlib.org/blas/faq.html

	which lists some URLs to optimized BLAS-implementations for
	some architectures. For an optimized version for AMD's Athlon
	and Opteron CPUs you should have a look at AMD's web-site
	(www.amd.com) and search for "acml".

	Compiler dependence
	-------------------
	In general, you need a version of the BLAS which was compiled
	by the Fortran compiler you are using to compile (parts of)
	the ALBERTA package. Sometimes it is possible to get around
	this restriction by linking in additional libraries, as shown
	in some of the platform dependent examples listed below.

        ALBERTA can be compiled without BLAS, in which case
        C-replacements are used instead of the BLAS-functions.

GPSKCA -- "Gibbs-Pool-Stockmeyer" or "Gibbs-King" algorithm for either
        bandwidth or profile reduction. Note that GPSKCA is a Fortran
        package which also needs BLAS.

gltools -- OpenGL toolkit
	We strongly recommend that you install the gltools package;
	you can fetch it from

	http://www.wias-berlin.de/software/gltools/

	gltools provides a more flexible graphical output than would
	be otherwise available with ALBERTA. 

	NOTE: you need at least gltools-2-4. Version 2-3 will _not_
	work. "configure" does not check for right version, it's up to
	yourself.

dxtools -- OpenDX toolkit
	The dxtools are an interface to the OpenDX visualization software.
	It may be downloaded from

	http://www.opendx.org/

	The visualization features are far more advanced than gltools, 
	however OpenDX is a huge project that takes time getting used to.
	The dxtools interface was designed to keep things simple and 
	intuitive.
	
GRAPE -- Graphics Programming Environment
	If GRAPE is present, the four programs alberta_grape22,
	alberta_grape33, alberta_movi22 and alberta_movi33 are
	compiled and installed in PREFIX/bin/. They can be used to
	display data-files created by ALBERTA during your numerical
	simulations.

	GRAPE is available from

	http://www.mathematik.uni-freiburg.de/IAM/Research/grape/GENERAL/

Silo -- Silo is a library developed at the Lawrence Livermore National
	Laboratory to handle portable storage of mesh and function data. It
	serves as an interface to MeshTV, an open source visualization 
	project also based at the LLNL.
	If the Silo library is present, the programs alberta_meshtv? are
	compiled and installed. They can be used to convert ALBERTA meshes 
	and DOF_REAL[_D]_VECS to Silo data files, which can then be 
	visualized using the MeshTV binary.

	MeshTV and Silo are available from

	http://www.llnl.gov/bdiv/meshtv/

GMV --  "General Mesh Viewer" is available from 

        http://www-xdiv.lanl.gov/XCM/gmv/GMVHome.html

        ALBERTA provides a routine "write_mesh_gmv()", which converts
        the mesh and the finite element functions to a format
        understood by "gmv". This is just a separate program, no
        external library needs to be linked in.

Geomview -- An ancient program, originally from the Geometry Center,
        available from

        http://sourceforge.net/projects/geomview/

        `./demo/src/Common/geomview-graphics.c' contains an interface
        from ALBERTA to Geomview. The demo-programs for parametric
        meshes, especially those for higher co-dimension, can make use
        of Geomview. `geomview-graphics.c' does not require any
        external libraries for linking. You need Geomview version
        >1.9.0, and -- for the visualization of higher dimensional
        meshes -- the `gvemod-ndview' add-on. Both are available from
        SourceForge.

Paraview -- For viewing the output from the alberta2paraviewXd converters.

	http://www.paraview.org


III) Configure Options
======================

"configure --help" will give you a summary of available options. The file
"INSTALL" contains generic configuration instructions and a description of
generic command line options for configure.

The remaining (i.e. ALBERTA specific) options are explained here:


1.) ALBERTA's installation paths
--------------------------------

The default installation prefix of the ALBERTA-package is the default
GNU installation prefix, i.e. `/usr/local/' Of course, you can change
the default behavior by using the "--prefix=PREFIX" switch when
running configure (see `./INSTALL'). However, the default layout is
like follows:

The libraries will go to

	/usr/local/lib/

the header-files and Makefile.alberta will end up in

	/usr/local/include/alberta/

The configured libtool-script and the Makefile-fragment for the
demo-suite will reside in

	/usr/local/libexec/alberta-VERSION/

and finally the demo package (under the name alberta-VERSION.demo.tar.gz,
where VERSION denotes the actual version of the alberta package) will
be copied to

	/usr/local/share/alberta/

This will be the layout after running "make install".


2.) Options affecting which versions of ALBERTA are built
---------------------------------------------------------

Compiling all flavours of the ALBERTA libraries takes a long time. The
ALBERTA libraries are all named

	libalberta_[fem_,gfx_]<DIM_OF_WORLD>d[_debug]



The options below control which of them are actually created.

  --disable-alberta-1     disable building of an ALBERTA library for Finite
                          Elements of dimension 1. (default: enabled)
  --disable-alberta-2     disable building of an ALBERTA library for Finite
                          Elements of dimension 2. (default: enabled)
  --disable-alberta-3     disable building of an ALBERTA library for Finite
                          Elements of dimension 3. (default: enabled)
  --enable-dim-of-world="DIM1 ... DIMk"
                          Compile ALBERTA libraries for DIM_OF_WORLD=DIM1,
                          ..., DIM_OF_WORLD=DIMk. Supported dimensions range
                          from 4 to 9. Dimensions are separated by spaces.
                          Note that this does not change the maximal mesh
                          dimension which remains at 3. The default is NOT to
                          compile for any dimension > 3.
  --disable-debug         disable building of ALBERTA libraries with debugging
                          information. (default: enabled)
  --disable-graphics      disable support for visualization, including all
                          add-ons which need graphics. (default: enabled)
  --disable-fem-toolbox   disable building of the actual FEM-toolbox. Primarily
                          meant for making the Fremen a little bit
                          happier. This will speed up compilation
                          drastically. Implies "--disable-graphics". Only the
                          core libraries libalberta_Nd[_debug] are built.

3.) Options controlling specific features
-----------------------------------------

  --enable-fortran-blas   Use BLAS routines for some basic linear algebra
                          stuff. (default: disabled)
  --disable-vector-basis-functions
                          Disable support for vector-valued basis functions.
                          If you leave this enabled ALBERTA supports
                          DIM_OF_WORLD-valued basis functions and scalar basis
                          functions. If you disable this feature ALBERTA only
                          supports scalar valued basis functions.
                          DIM_OF_WORLD-valued basis functions play an
                          important role, e.g. for the discretisation of
                          problems involving H(div) (Maxwell equations ...)
                          and, e.g., for stable mixed discretisations for the
                          Stokes-problem. Building ALBERTA with support for
                          vector valued basis functions increases the
                          compile-time, otherwise it should not have any
                          measureable performance impact, therefore this
                          feature is normally ENABLED. Disable it to reduce
                          the compile time if you do not need this feature.
                          (default: enabled)
  --disable-chained-basis-functions
                          Disable support for chains of basis-functions.
                          ALBERTA has support for forming direct sums of
                          FE-spaces. This is useful, e.g., to implement
                          certain stable mixed discretisations for the
                          Stokes-problem (P1+Bubble = Mini, P1+FaceBubble =
                          Bernardi-Raugel, weak slip b.c.). Keeping this
                          feature enabled will have a slight performance
                          impact, you may disable it if you do not need this
                          feature. (default: enabled)
 
4.) Options controlling the search-path for external libraries
--------------------------------------------------------------

See also "External Packages" above.

For each external package PKG the configure script provides the
following options:

  --without-PKG
	Prohibit the use of this package, even if it is installed on
	your system.

  --with-PKG-name=NAME
        Alter the default name of the package,
        e.g. "--with-opengl-name=MesaGL" or "--with-blas-name=cxml".
	NOTE: it is possible to specify more than one library. For
	example on (some versions of?) Solaris the BLAS-library is
	called "libsunperf.so". If you want to link with this library
	using gcc and g77, then you need to specify

	"--with-blas-name=sunperf -lfui -lfsu -lsunmath"

	Note the "-l" in front of the additional libraries.
	(of course, in addition to --with-blas-name you also need
	--with-blas-lib=WHATEVER)

  --with-PKG-dir=DIR
	Search for header-files and for the library itself below DIR,
	e.g. "--with-gltools-dir=/foo/bar/gltools-2.4".

  --with-PKG-lib=DIR
	Search for the library below DIR,
	e.g. "--with-blas-lib=/usr/people/claus/software/lib/".

  --with-PKG-headers=DIR
	Use DIR as search-path for the include-files for PKG,
	e.g. "--with-opengl-include=/usr/people/claus/software/include/".

The following quotes the relevant fragment of the online-help obtained
by running "configure --help". Please see the file ./INSTALL for the
notation (e.g. PREFIX, EPREFIX etc.).

  --with-blas-name=NAME   use NAME as the name of the blas library (without
                          leading "lib" prefix and trailing suffix). Default:
                          "blas"
  --with-blas-lib=DIR     use blas library below directory DIR (default:
                          EPREFIX/lib/)
  --without-OpenGL        disable use of package OpenGL (default: auto-detect)
  --with-OpenGL-name=NAME use NAME as the name of the OpenGL library (without
                          leading "lib" prefix and trailing suffix). Default:
                          "GL"
  --with-OpenGL-dir=DIR   use OpenGL library (and headers) below directory DIR
                          (no default)
  --with-OpenGL-lib=DIR   use OpenGL library below directory DIR (default:
                          ${x_libraries})
  --with-OpenGL-headers=DIR
                          use OpenGL include files below directory DIR
                          (default: ${x_includes}/GL -I/usr/include/GL/)
  --without-gltools       disable use of package gltools (default: auto-detect)
  --with-gltools-name=NAME
                          use NAME as the name of the gltools library (without
                          leading "lib" prefix and trailing suffix). Default:
                          "gltools"
  --with-gltools-dir=DIR  use gltools library (and headers) below directory
                          DIR (no default)
  --with-gltools-lib=DIR  use gltools library below directory DIR (default:
                          EPREFIX/lib/)
  --with-gltools-headers=DIR
                          use gltools include files below directory DIR
                          (default: PREFIX/include/)
  --without-OpenDX        disable use of package OpenDX (default: auto-detect)
  --with-OpenDX-name=NAME use NAME as the name of the OpenDX library (without
                          leading "lib" prefix and trailing suffix). Default:
                          "DXL"
  --with-OpenDX-dir=DIR   use OpenDX library (and headers) below directory DIR
                          (no default)
  --with-OpenDX-lib=DIR   use OpenDX library below directory DIR (default:
                          EPREFIX/lib/)
  --with-OpenDX-headers=DIR
                          use OpenDX include files below directory DIR
                          (default: PREFIX/include/)
  --without-grape         disable use of package grape (default: auto-detect)
  --with-grape-name=NAME  use NAME as the name of the grape library (without
                          leading "lib" prefix and trailing suffix). Default:
                          "gr"
  --with-grape-dir=DIR    use grape library (and headers) below directory DIR
                          (no default)
  --with-grape-lib=DIR    use grape library below directory DIR (default:
                          EPREFIX/lib/)
  --with-grape-headers=DIR
                          use grape include files below directory DIR
                          (default: PREFIX/include/)
  --without-silo          disable use of package silo (default: auto-detect)
  --with-silo-name=NAME   use NAME as the name of the silo library (without
                          leading "lib" prefix and trailing suffix). Default:
                          "silo"
  --with-silo-dir=DIR     use silo library (and headers) below directory DIR
                          (no default)
  --with-silo-lib=DIR     use silo library below directory DIR (default:
                          EPREFIX/lib/)
  --with-silo-headers=DIR use silo include files below directory DIR (default:
                          PREFIX/include/)

IV. Compiler flags
==================

The default flags for the optimized libraries are "-O3" when using gcc
and "-O" otherwise.

The default flags for the debugging-enabled libraries are
"-O0 -ggdb -fno-inline -fno-builtin" with gcc and "-g" otherwise.

The relevant environment- respectively make-variables are listed at
the end of the online-help provided by running `configure --help'.
They include:

CFLAGS
  C-compiler flags used for _both_, optimized and debug enabled libraries

ALBERTA_OPTIMISM_CFLAGS
  C-compiler flags used for the optimized library. They are prepended
  to the CFLAGS variable.

ALBERTA_DEBUG_CFLAGS
  C-compiler flags used for the debug enabled library. They are
  prepended to the CFLAGS variable.

ALBERTA_FFLAGS
  Fortran-compiler flags. They are separated from the CFLAGS because
  in general the Fortran compiler can come from a different vendor
  than the C-compiler in which case the flags understood by the
  Fortran compiler are different from the flags understood by the
  C-compiler.

You have to consult the documentation for the compiler(s) you are using
to determine the appropriate switches for your setup.

There are three ways to set those flags:

a.) arguments to configure
b.) environment variables (have to be defined _before_ running configure)
c.) arguments to make

We recommend using a.).

Examples:
---------

1.) gcc with a Pentium 4

  ./configure [OTHER OPTIONS] \
     CFLAGS="-march=pentium4 -mfpmath=sse" \
     FFLAGS="-march=pentium4 -mfpmath=sse" 
  make
  make install

or (assuming a Bourne-shell)

  CFLAGS="-march=pentium4 -mfpmath=sse"
  FFLAGS="-march=pentium4 -mfpmath=sse" 
  export CFLAGS FFLAGS

  ./configure [OTHER OPTIONS]
  make
  make install

or

  ./configure [OTHER OPTIONS]
  make CFLAGS="-march=pentium4 -mfpmath=sse" \
    FFLAGS="-march=pentium4 -mfpmath=sse" 
  make install

2.) Pentium 4 with icc and with Intel "math kernel library" (BLAS
implementation)

  ./configure [OTHER OPTIONS] \
     CC=icc CFLAGS="-xW" ALBERTA_OPTIMIZE_CFLAGS="-O3" \
     ALBERTA_DEBUG_CFLAGS="-O0 -g" \
     F77=ifc FFLAGS="-xW -O3" \
     --with-blas-name=mkl --with-blas-lib=/opt/intel/mkl61/lib/32/
  make
  make install

or set environment variables or use arguments to "make" as shown in
example 1.)
