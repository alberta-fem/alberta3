#
# disable building of $1_library (dimension)
#
#
# NOTE: it is a bad idea to remove the unneeded Makefiles from the
# config-files, it breaks "make distclean"
#
# Arguments:
# $1: dimension (integer value)
# $2: if given, the default value. The default for the default is 1.
#
#
AC_DEFUN([ALBERTA_DIMENSION_DISABLE],
[m4_if($#,2,
	[m4_define([DEFAULT],[$2])],
	[m4_define([DEFAULT],[1])])
m4_if(DEFAULT,1,
	[ALBERTA_ENABLE_FLAG([alberta-$1],
		[disable building of an ALBERTA library for Finite Elements of dimension $1.],
		1, BUILD_ALBERTA_$1)],
	[ALBERTA_ENABLE_FLAG([alberta-$1],
		[EXPERIMENTAL: enable building of an ALBERTA library for Finite Elements of dimension $1.],
		0, BUILD_ALBERTA_$1)])
m4_if(m4_eval(($1) < 4),1,
	[AC_CONFIG_FILES([alberta/src/alberta_$1d/Makefile])
	 AC_CONFIG_FILES([alberta/src/alberta_$1d_debug/Makefile])
	 AC_CONFIG_FILES([alberta/src/alberta_$1d_profile/Makefile])])
])

#
# Set the necessary defines and conditinonals for the given dimension.
#
# Arguments:
# $1 dimension (DIM_OF_WORLD)
#
AC_DEFUN([ALBERTA_DEFINE_DIMENSION],
[AC_SUBST(BUILD_ALBERTA_$1)
AM_CONDITIONAL(BUILD_ALBERTA_$1, [test "z${BUILD_ALBERTA_$1}" = "z1"])])
