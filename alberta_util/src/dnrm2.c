#include "alberta_util_intern.h"

double dnrm2(int n, const double *x, int ix)
{
  double result = 0.0;
  
  if (ix == 1) {
    while (n--) {
      result += *x * *x;
      x ++;
    }
  } else {
    while (n--) {
      result += *x * *x;
      x += ix;
    }
  }
  return sqrt(result);
}
