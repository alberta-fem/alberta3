/*--------------------------------------------------------------------------*/
/* ALBERTA_UTIL:  tools for messages, memory allocation, parameters, etc.   */
/*                                                                          */
/* file:     divers.c                                                       */
/*                                                                          */
/* description:  generating filenames                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"

const char *generate_filename(const char *path, const char *fn, int ntime)
{
  static char  name[1024];
  char         *cp = name;

  *cp = 0;
/*---  adjust path   -------------------------------------------------------*/
  if (path == NULL || *path == '\0')
    strcpy(name, "./");
  else if (*path == '~')
  {
    strcpy(name, getenv("HOME"));
    strcat(name, path+1);
  }
  else
    strcpy(name, path);

  while (*cp)
    cp++;
  cp--;
  if (*cp != '/')
    strcat(cp, "/");

/*---  append  filename  ---------------------------------------------------*/
  strcat(cp, fn);

/*---  finally append normalized iteration counter  ------------------------*/
  while (*cp) cp++;
  sprintf(cp, "%06d", ntime);

  return((const char *) name);
}
