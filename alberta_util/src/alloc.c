/*--------------------------------------------------------------------------*/
/* ALBERTA_UTIL:  tools for messages, memory allocation, parameters, etc.   */
/*                                                                          */
/* file:     alloc.c                                                        */
/*                                                                          */
/* description:  utilities for memory allocation                            */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#if defined(__MACH__)
  #include <stdlib.h>
#else
  #include <malloc.h>
  #include <stdlib.h>
#endif

#include "alberta_util.h"

#define MALLOC(size)         malloc(size)
#define REALLOC(ptr, size)   (ptr) ? realloc(ptr, size) : malloc(size)
#define FREE(ptr)            free(ptr)
#define CALLOC(size, elsize) calloc(size, elsize)

#define ALLOC_ERR(fct, file, line)\
if (fct && file) ERROR_EXIT("called by %s in %s, line %d\n", fct, file, line);\
else if (fct)    ERROR_EXIT("called by %s, (unknown filename)\n", fct);\
else if (file)   ERROR_EXIT("called in %s, line %d\n", file, line);\
else             ERROR_EXIT("location unknown\n")

WORKSPACE *get_workspace(size_t size, const char *fct, const char *f, int l)
{
  WORKSPACE *ws = (WORKSPACE *)alberta_alloc(sizeof(WORKSPACE), fct, f, l);
  ws->work = alberta_alloc(size, fct, f, l);
  ws->size = size;

  return ws;
}

WORKSPACE  *realloc_workspace(WORKSPACE *ws, size_t new_size,
			      const char *fct, const char *file, int line)
{
  WORKSPACE  *workspace = ws;

  if (!workspace) {
    workspace = (WORKSPACE *)alberta_alloc(sizeof(WORKSPACE), fct, file, line);
    workspace->work = NULL;
  }

  if (!workspace->work)
  {
    workspace->work = alberta_alloc(new_size, fct, file, line);
    workspace->size = new_size;
  } else if (workspace->size < new_size) {
    workspace->work = alberta_realloc(workspace->work, workspace->size,
				      new_size, fct, file, line);
    workspace->size = new_size;
  }
  return workspace;
}

void clear_workspace(WORKSPACE *ws)
{
  if (!ws) return;
  alberta_free(ws->work, ws->size);
  ws->work = NULL;
  ws->size = 0;
}

void free_workspace(WORKSPACE *ws)
{
  if (!ws) return;
  alberta_free(ws->work, ws->size);
  MEM_FREE(ws, 1, WORKSPACE);
}

/*--------------------------------------------------------------------------*/
/*  matrix (de-)allocation routines: 					    */
/*  free_matrix deallocates such a matrix				    */
/*--------------------------------------------------------------------------*/


void **alberta_matrix(int nr, int nc, size_t size,
		      const char *fct, const char *file, int line)
{
  int       i;
  size_t    row_length = nc*size;
  char      **mat, *mrows;

  mat = (char **)alberta_alloc(nr*sizeof(char *), fct, file, line);
  mrows = (char *)alberta_alloc(nr*nc*size, fct, file, line);

  for(i = 0; i < nr; i++)
    mat[i] = mrows + i*row_length;

  return (void **)mat;
}

void  free_alberta_matrix(void **ptr, int nr, int nc, size_t size)
{
  if (ptr) {
    alberta_free(ptr[0], nr*nc*size);
  }
  alberta_free(ptr, nr*sizeof(char *));
}

void clear_alberta_matrix(void **ptr, int nr, int nc, size_t size)
{
  memset(ptr[0], 0, size*nr*nc);
}

void ***alberta_3array(int nr, int nc, int nd, size_t size,
		       const char *fct, const char *file, int line)
{
  int    i, j;
  size_t depth_length = nd*size;
  char   ***array, *mdepth;

  array = (char ***)alberta_matrix(nr, nc, sizeof(char *), fct, file, line);
  mdepth = (char *)alberta_alloc(nr*nc*nd*size, fct, file, line);

  for(i = 0; i < nr; i++) {
    for(j = 0; j < nc; j++) {
      array[i][j] = mdepth;
      mdepth += depth_length;
    }
  }

  return (void ***)array;
}

void free_alberta_3array(void ***array, int nr, int nc, int nd, size_t size)
{
  if (array) {
    if (array[0]) {
      alberta_free(array[0][0], nr*nc*nd*size);
    }
    alberta_free(array[0], nr*nc*sizeof(void *));
  }
  alberta_free(array, nr*sizeof(void **));
}

void clear_alberta_3array(void ***array, int nr, int nc, int nd, size_t size)
{
  memset(array[0][0], 0, nr*nc*nd*size);
}

/* Allocate memory for a 4-tensor */
void ****alberta_4array(int n0, int n1, int n2, int n3, size_t size,
			const char *fct, const char *file, int line)
{
  int    i, j, k;
  size_t depth_length = n3*size;
  char   ****array, *mdepth;

  array =
    (char ****)alberta_3array(n0, n1, n2, sizeof(char *), fct, file, line);
  mdepth = (char *)alberta_alloc(n0*n1*n2*n3*size, fct, file, line);

  for(i = 0; i < n0; i++) {
    for(j = 0; j < n1; j++) {
      for(k = 0; k < n2; k++) {
	array[i][j][k] = mdepth;
	mdepth += depth_length;
      }
    }
  }

  return (void ****)array;
}

void
free_alberta_4array(void ****array, int n0, int n1, int n2, int n3, size_t size)
{
  if (array) {
    if (array[0]) {
      if (array[0][0]) {
	alberta_free(array[0][0][0], n0*n1*n2*n3*size);
      }
    }
  }
  free_alberta_3array((void ***)array, n0, n1, n2, sizeof(char *));
}

void clear_alberta_4array(void ****array,
			  int n0, int n1, int n2, int n3, size_t size)
{
  memset(array[0][0][0], 0, n0*n1*n2*n3*size);
}

