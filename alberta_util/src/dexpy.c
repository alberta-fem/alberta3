#include "alberta_util_intern.h"

void dexpy(int n, const double *x, int ix, double *y, int iy)
{
  DEXPY_F77(&n, x, &ix, y, &iy);
  return;
}
