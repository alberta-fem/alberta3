#include "alberta_util_intern.h"

void dxpay(int n, const double *x, int ix, double alpha, double *y, int iy)
{
  DXPAY_F77(&n, x, &ix, &alpha, y, &iy);
  return;
}
