#include "alberta_util_intern.h"

void dmxpy(int n, const double *x, int ix, double *y, int iy)
{
  DMXPY_F77(&n, x, &ix, y, &iy);
  return;
}
