#include "alberta_util_intern.h"

void daxpy(int n, double alpha, const double *x, int ix, double *y, int iy)
{
  DAXPY_F77(&n, &alpha, x, &ix, y, &iy);
  return;
}
