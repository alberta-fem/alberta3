#include "alberta_util_intern.h"

void dscal(int n, double alpha, double *x, int ix)
{
  DSCAL_F77(&n, &alpha, x, &ix);
}
