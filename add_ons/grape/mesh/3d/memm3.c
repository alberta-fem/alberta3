/*
 * grape-module "memm3.c"
 *
 * Description       : memory methods: new-instance, free, xdr, ...
 *
 * Author            : Ralf Neubauer
 *
 * Copyright (c) 1996  by Universitaet Bonn
 *                        Institut fuer Angewandte Mathematik
 *                        Sonderforschungsbereich 256
 *                        D-53115 Bonn
 *
 * $Id: memm3.c,v 1.1.1.1 2005/04/13 15:56:40 dani Exp $
 * $Log: memm3.c,v $
 * Revision 1.1.1.1  2005/04/13 15:56:40  dani
 * Start of the ALBERTA 2.0 repository
 *
 * Revision 1.1  2003/06/24 14:06:58  claus
 * Initial revision.
 *
 * Revision 1.17  2000/08/28 14:58:14  wu
 * restructured mesh class hierarchy
 *
 * Revision 1.16  2000/06/07  14:45:37  wu
 * error corrected
 *
 * Revision 1.15  2000/06/07  14:09:03  wu
 * error corrected
 *
 * Revision 1.14  2000/06/07  13:43:03  wu
 * revised mscalar functions
 *
 * Revision 1.13  2000/06/06  13:17:37  wu
 * corrected copy-function
 *
 * Revision 1.12  2000/06/05 19:59:59  wu
 * added 'synthetic functions', therefore revised function list management
 *
 * Revision 1.11  2000/01/26  16:29:06  wu
 * added forgot superclass call to xdr
 *
 * Revision 1.10  1999/05/05 16:31:04  wu
 * added get-min-max
 *
 * Revision 1.9  1999/03/26  16:01:36  wu
 * softcopy and get-object added
 *
 * Revision 1.8  1999/03/25  15:38:53  wu
 * function selector unification changes
 *
 * Revision 1.7  1999/03/25  12:35:09  wu
 * functionality moved to GenMesh
 *
 * Revision 1.6  1999/02/05  18:20:03  wu
 * remove functions of free
 *
 * Revision 1.5  1999/01/26  16:07:37  wu
 * xdr function selector
 *
 * Revision 1.4  1999/01/19  14:25:02  wu
 * added new-instance and free
 *
 * Revision 1.3  1998/08/21  14:21:56  wu
 * add/remove-function methods added
 *
 * Revision 1.2  1996/11/12  12:12:27  wu
 * warning added: use own "xdr"
 *
 * Revision 1.1  1996/11/11  15:24:55  wu
 * Initial revision
 *
 *
 */

#include <grape.h>

static ELEMENT3D *helement_to_element (ELEMENT3D *el)
{
  if (!el)
    return NULL;
  el->parent = NULL;
  el->vinh = NULL;
  el->level = 0;
  el->ref_rule = 0;
  el->has_children = FALSE;
  return el;
}

static ELEMENT3D *first_macro (GENMESH3D *self, MESH_ELEMENT_FLAGS flags)
{
  return helement_to_element
    ((*((MESH3D *)self)->first_element)((MESH3D *)self, flags));
}

static ELEMENT3D *next_macro (ELEMENT3D *el, MESH_ELEMENT_FLAGS flags)
{
  return helement_to_element
    ((*((MESH3D *)el->mesh)->next_element)(el, flags));
}

static ELEMENT3D *get_child (HELEMENT3D *el, MESH_ELEMENT_FLAGS flags)
{
  return NULL;
}

MESH3D *mesh3d_init_mesh (void)
{ 
  MESH3D *self;

  self = (MESH3D *)START_METHOD (G_INSTANCE);
  ASSURE (self, "", END_METHOD (NULL));
 
  self->max_level = 0;
  self->level_of_interest = 0;

  self->first_macro = first_macro;
  self->next_macro = next_macro;
  self->first_child = get_child;
  self->next_child = get_child;

  END_METHOD(self);
}

MESH3D *mesh3d_copy_mesh (MESH3D *copy)
{
  MESH3D *self;

  self = (MESH3D *)START_METHOD (G_INSTANCE);
  ASSURE (self, "", END_METHOD (NULL));
 
  copy->first_element = self->first_element;
  copy->next_element = self->next_element;

  END_METHOD (copy);
}
