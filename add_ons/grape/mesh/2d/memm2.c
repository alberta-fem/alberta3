/*
 * grape-module "memm2.c"
 *
 * Description       : memory methods: new-instance, free, xdr, ...
 *
 * Author            : Ralf Neubauer
 *
 * Copyright (c) 1996  by Universitaet Bonn
 *                        Institut fuer Angewandte Mathematik
 *                        Sonderforschungsbereich 256
 *                        D-53115 Bonn
 *
 * $Id: memm2.c,v 1.1.1.1 2005/04/13 15:56:40 dani Exp $
 * $Log: memm2.c,v $
 * Revision 1.1.1.1  2005/04/13 15:56:40  dani
 * Start of the ALBERTA 2.0 repository
 *
 * Revision 1.1  2003/06/24 14:06:57  claus
 * Initial revision.
 *
 * Revision 1.19  2000/08/28 14:50:59  wu
 * restructured mesh class hierarchy
 *
 * Revision 1.18  2000/06/07 14:45:37  wu
 * error corrected
 *
 * Revision 1.17  2000/06/07  14:09:03  wu
 * error corrected
 *
 * Revision 1.16  2000/06/07  13:42:45  wu
 * revised mscalar functions
 *
 * Revision 1.15  2000/06/06  13:15:29  wu
 * corrected copy-function
 *
 * Revision 1.14  2000/06/05 19:58:50  wu
 * added 'synthetic functions', thus revised function list management
 *
 * Revision 1.13  2000/01/26  16:25:03  wu
 * added forgot superclass call in xdr
 *
 * Revision 1.12  1999/08/23 12:12:41  haasdonk
 * bug in get-min-max fixed
 *
 * Revision 1.11  1999/05/05  16:29:54  wu
 * added get-min-max
 *
 * Revision 1.10  1999/03/26  16:01:12  wu
 * softcopy and get-object added
 *
 * Revision 1.9  1999/03/25  13:31:43  wu
 * GenMesh conversion error corrected
 *
 * Revision 1.8  1999/03/25  13:23:36  wu
 * error corrected
 *
 * Revision 1.7  1999/03/25  12:34:29  wu
 * functionality moved to GenMesh
 *
 * Revision 1.6  1999/02/05  18:18:45  wu
 * remove functions on free
 *
 * Revision 1.5  1999/01/26  16:07:56  wu
 * xdr funciton selector
 *
 * Revision 1.4  1999/01/19  14:25:17  wu
 * added new-instance and free
 *
 * Revision 1.3  1998/08/18  12:58:19  wu
 * new methods add/remove-function
 *
 * Revision 1.2  1997/08/30  11:05:44  wu
 * error corrected
 *
 */

#include <grape.h>

static ELEMENT2D *helement_to_element (ELEMENT2D *el)
{
  if (!el)
    return NULL;
  el->parent = NULL;
  el->vinh = NULL;
  el->level = 0;
  el->ref_rule = 0;
  el->has_children = FALSE;
  return el;
}

static ELEMENT2D *first_macro (GENMESH2D *self, MESH_ELEMENT_FLAGS flags)
{
  return helement_to_element
    ((*((MESH2D *)self)->first_element)((MESH2D *)self, flags));
}

static ELEMENT2D *next_macro (ELEMENT2D *el, MESH_ELEMENT_FLAGS flags)
{
  return helement_to_element
    ((*((MESH2D *)el->mesh)->next_element)(el, flags));
}

static ELEMENT2D *get_child (HELEMENT2D *el, MESH_ELEMENT_FLAGS flags)
{
  return NULL;
}

MESH2D *mesh2d_init_mesh (void)
{ 
  MESH2D *self;

  self = (MESH2D *)START_METHOD (G_INSTANCE);
  ASSURE (self, "", END_METHOD (NULL));
 
  self->max_level = 0;
  self->level_of_interest = 0;

  self->first_macro = first_macro;
  self->next_macro = next_macro;
  self->first_child = get_child;
  self->next_child = get_child;

  END_METHOD(self);
}

MESH2D *mesh2d_copy_mesh (MESH2D *copy)
{
  MESH2D *self;

  self = (MESH2D *)START_METHOD (G_INSTANCE);
  ASSURE (self, "", END_METHOD (NULL));
 
  copy->first_element = self->first_element;
  copy->next_element = self->next_element;

  END_METHOD (copy);
}
