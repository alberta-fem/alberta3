/* to be called from C code */ 

#ifdef __cplusplus
extern "C" {
#endif

int dxexport(MESH *mesh, const char *filename, 
	     REAL time, const DOF_REAL_VEC * const *vecs, int n_vecs);
void init_export_dof_admin(MESH *mesh);
#ifdef __cplusplus
}
#endif
