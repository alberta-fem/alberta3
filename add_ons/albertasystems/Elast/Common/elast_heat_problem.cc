/*prob 7: Spannung zu vorgegebener temperatur, ex. Lsg polynom (p'(x)p(y),p(x)p'(y)),  p=x^2(1-x)^2***
**************************************************************************************/

  REAL p(REAL x)
{
  return(x*x*(1-x)*(1-x));
/*  return(-x*x*x*x*x/5 + x*x*x/3);*/
}
/// const REAL a=100.0; Wat is datt denn??

  REAL pprime(REAL x)
{
/*  return(x*x*(1-x)*(1-x));*/
  return(2.0*x*(1.0-x)*(1.0-2.0*x));
}
 REAL p2prime(REAL x)
{
        return(12.0*x*x-12.0*x+2.0);
/*  return(2.0*x*(1-x)*(1-2*x));*/
}

static REAL    th_ex7(const REAL_D x)
{

  return(/*623.0 +*/ a*(p2prime(x[0])*p(x[1]) +  p2prime(x[1])*p(x[0])) );
}

#if DIM_OF_WORLD==2




static    REAL *grd_th_ex7(const REAL_D x, REAL t)
{
  static REAL_D grad_th = {0.0,  0.0};
  static REAL c1=0.0;
        REAL alpha = 1.55E-5;
        if (!c1)
                c1=alpha*(3*prob_data->lambda + 2*prob_data->mu)/(prob_data->lambda
                        + 2*prob_data->mu);
    grad_th[0] = 0.0;
    grad_th[1] = 0.0;/*dummy*/
 return(grad_th);
}




#endif


PROB_DATA_DIFF prob_data_heat_polynomial =    
     { 
	nil, nil,        /* Diffusionsmatrix, order0 werden ueberschrieben!!*/
	   nil		//massfunc
	 , 1 		//constflag
	,{0.0, 0.077, 0.084,/*latent_heat pearl, Martensit*/ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}, //phys. konstanten. Feld is eher f. phase gedacht  
    		/*rhs function:*/
    		nil, th_ex7, nil /*gth_D_6*/,nil /* gth_N_5*/,
    		th_ex7, grd_th_ex7,              /* th_ex, grd_th_ex */
        	};
PROB_DATA_DIFF prob_data_heat_const =    
        {
        nil, nil,        /* Diffusionsmatrix, order0 werden ueberschrieben!!*/
	   nil		//massfunc
	   , 1 		//constflag
	,{0.0, 0.077, 0.084,/*latent_heat pearl, Martensit*/    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
		}, //phys. konstanten. Feld is eher f. phase gedacht  
    		nil, th0, nil /*gth_D_6*/,nil /* liefert Nullen*/,
    		th0, grd_th_ex7,              /* th_ex, grd_th_ex nicht dafuer gemacht aber ok*/
        };
