#include <alberta.h>
#include "newalberta.hh"
#include "heat_problem.hh"
#include "operator_parts.hh"

//#include "header mit PROBLEM_DATA - Definition" muss vor Einbindung passiert sein
static const  PROB_DATA_DIFF* prob_data_heat=nil;
//glaub das ist obsolet

const REAL  Celsius = 273.0;  /* th value of 0 degree Celsius */

 REAL th0(const REAL_D x)            /* initial temperature */
{
  return(723.0/*+Celsius*/);
}

 REAL gth_D(const REAL_D x, REAL t) /* external temp */
{
  return(20.0+Celsius);/*dies dient nicht als Dirichlet-Bedingung, sondern Aussentemperatur f die Robin-Bedi
ngung!*/
}


/*static*/ REAL gth_N_3(const REAL_D x, REAL t, S_CHAR bound)  /* Robin coef   */
{
  FUNCNAME("gth_N_3");

  if (bound == -1 || bound == -6)
    return(0.1);
  else
    return(0.01);
}


PROB_DATA_DIFF prob_data_stdheat =        
{
        nil
	,nil
		, nil       /*  order0 u werden ueberschrieben!!*/
	, DIFFMAT_PW_CONST|DIFFMAT_ISOTROP|MASS_CONST
	,{0.0, 0.077, 0.084,/*latent_heat pearl, Martensit*/ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}, //phys. konstanten. Feld is eher f. phase gedacht  
         nil , th0,gth_D, gth_N_3, //f_nosource ist eigentlich zu nix gut
        
	nil , nil,              /* th_ex, grd_th_ex */
        };


void init_problem_stdheat(const PROB_DATA_DIFF **return_prob_data_heat)
{
  FUNCNAME("init_problem_stdheat");
	REAL diffkoeff=1.0; //....naja
 prob_data_stdheat.diffmat = new constmat(diffkoeff); 
// prob_data_stdheat.flags = DIFFMAT_PW_CONST; 
// hier nach is constant. also hier setzen
  /*prob_data->*/prob_data_heat = *return_prob_data_heat 
	=(const PROB_DATA_DIFF *)&prob_data_stdheat;

}
