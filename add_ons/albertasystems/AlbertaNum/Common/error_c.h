/* file error_c.c */
REAL L2_err_c(REAL (*u)(const REAL_D), const DOF_REAL_VEC *uh,
              const QUAD *quad, REAL *(*rw_err_el)(EL *),
              REAL *max_l2_err2, REAL c);
REAL H1_err_c(const REAL *(*grd_u)(const REAL_D), const DOF_REAL_VEC *uh,
              const QUAD *quad, REAL *(*rw_err_el)(EL *),
              REAL *max_h1_err2, REAL c);
REAL L2_err_c_d(const REAL *(*u)(const REAL_D, REAL_D),
                const DOF_REAL_D_VEC *uh,
                const QUAD *quad, REAL *(*rw_err_el)(EL *),
                REAL *max_l2_err2, REAL c);
REAL H1_err_c_d(const REAL_D *(*grd_u)(const REAL_D, REAL_DD),
                const DOF_REAL_D_VEC *uh, const QUAD *quad,
                REAL *(*rw_err_el)(EL *), REAL *max_h1_err2, REAL c);
