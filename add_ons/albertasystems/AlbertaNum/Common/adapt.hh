// Standard-marking Funktionen
extern U_CHAR okmarking(MESH *mesh, ADAPT_STAT *adapt);
extern U_CHAR nomarking(MESH *mesh, ADAPT_STAT *adapt);

// Zukunftsmusik
#if 0
struct adapt_stat
{
  const char  *name;
  REAL        tolerance;
  REAL        p;                         /* power in estimator norm        */
  int         max_iteration;
  int         info;

  REAL   (*estimate)(MESH *mesh, ADAPT_STAT *adapt);
  REAL   (*get_el_est)(EL *el);          /* local error estimate           */
  REAL   (*get_el_estc)(EL *el);         /* local coarsening error estimate*/
  U_CHAR (*marking)(MESH *mesh, ADAPT_STAT *adapt);

  void   *est_info;                      /* estimator parameters           */
  REAL   err_sum, err_max;               /* sum and max of el_est          */

  void   (*build_before_refine)(MESH *mesh, U_CHAR flag);
  void   (*build_before_coarsen)(MESH *mesh, U_CHAR flag);
  void   (*build_after_coarsen)(MESH *mesh, U_CHAR flag);
  void   (*solve)(MESH *mesh);

  int    refine_bisections;
  int    coarsen_allowed;                /* 0 : 1                          */
  int    coarse_bisections;

  int    strategy;                       /* 1=GR, 2=MS, 3=ES, 4=GERS       */
  REAL   MS_gamma, MS_gamma_c;           /* maximum strategy               */
  REAL   ES_theta, ES_theta_c;           /* equidistribution strategy      */
  REAL   GERS_theta_star, GERS_nu, GERS_theta_c;  /* willy's strategy      */
};


struct adapt_instat
{
  const char  *name;

  ADAPT_STAT adapt_initial[1];
  ADAPT_STAT adapt_space[1];

  REAL   time;
  REAL   start_time, end_time;
  REAL   timestep;

  void   (*init_timestep)(MESH *mesh, ADAPT_INSTAT *adapt);
  void   (*set_time)(MESH *mesh, ADAPT_INSTAT *adapt);
  void   (*one_timestep)(MESH *mesh, ADAPT_INSTAT *adapt);
  REAL   (*get_time_est)(MESH *mesh, ADAPT_INSTAT *adapt);
  void   (*close_timestep)(MESH *mesh, ADAPT_INSTAT *adapt);

  int    strategy; 
  int    max_iteration;

  REAL   tolerance;
  REAL   rel_initial_error;
  REAL   rel_space_error;
  REAL   rel_time_error;
  REAL   time_theta_1;
  REAL   time_theta_2;
  REAL   time_delta_1;
  REAL   time_delta_2;
  int    info;
};
#endif
