/*copied from ... for gettig torus wall trafo.*/
/*******************************************************************************
 *
 * ALBERTA:  an Adaptive multi Level finite element toolbox  using
 *           Bisectioning refinement and Error control by Residual
 *           Techniques
 *
 * www.alberta-fem.de
 *
 * File:     ellipt-periodic.c
 *
 * Description:  solver for a simple Poisson equation on a periodic domain:
 *
 *                 -\Delta_\Gamma u = f
 *                                u = g  on \partial\Gamma
 *
 * The computational domain is either a topological torus or a
 * topological Klein's bottle (or whatever the name of it may be in
 * 3d).
 *
 ******************************************************************************
 *
 * author(s): Claus-Justus Heine
 *            Abteilung fuer Angewandte Mathematik
 *            Albert-Ludwigs-Universitaet Freiburg
 *            Hermann-Herder-Str. 10
 *            79104 Freiburg
 *            Germany
 *            Claus.Heine@Mathematik.Uni-Freiburg.DE
 *
 * (c) by C.-J. Heine (2006-2007)
 *
 * Based on the famous ellipt.c demo-program by K.G. Siebert and A. Schmidt.
 *
 ******************************************************************************/


/*******************************************************************************
 * initialization of wall-transformations.
 ******************************************************************************/
AFF_TRAFO *init_wall_trafos(MESH *mesh, MACRO_EL *mel, int wall);
