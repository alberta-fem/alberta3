void graphics(MESH *mesh,  REAL (*get_el_val)( EL *el),  const DOF_REAL_VEC  * const realvec,
   		const DOF_REAL_D_VEC *  vecvec,
 			int wnr);
void 	save_tensorcomps(DOF_REAL_VEC **strain, char *name);
 REAL get_el_mark(EL *el);
