
/*!error estimator for parabolic problems. reuses nearly evereythig from ellipt_est.
Implements alberta original in our C++ ways*/

class parab_est :  public ellipt_est
{
	friend class sys_line_est;//damit sys_line_est calc u set_at_el machenkann.
   public:
  	parab_est(const char *const name, const fem_base* discrete_prob
				,const  REAL *const timestep,int degree);
 	parab_est(const char *const name,const  fem_base* discrete_prob,
                       DOF_REAL_VEC   * *const err_estimate, DOF_REAL_VEC   ** const  err_c_estimate,
						   const REAL * const timestep,REAL C[4],int degree);
	~parab_est()
		{};
protected:
	virtual int set_at_el(const EL_INFO * const el_info,REAL eval_time);
//	virtual REAL calc(const REAL* const lambda, const int qp);
	virtual REAL calc(const REAL*)
		{FUNCNAME("parab_est::calc(const REAL*)");
		ERROR_EXIT("unimplemented Method");
		return 0.0;
		};


private:
};
