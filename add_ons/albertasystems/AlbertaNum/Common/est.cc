#include <alberta.h>
#if (defined(ALBERTA_VER) && (ALBERTA_VER==2))
//# include "alberta1_wrapper.h"
#endif

#include "newalberta.hh"
/****************************************************************************/
#include "do_on_el.hh"
#include "prototype_fem_elfunc.hh"
#include "fem_elfunc.hh"
#include "do_elfunc_on_el.hh"
//#include "public_do_elfunc_on_el.hh"
#include "fem_base.hh"
#include "est.hh"

const  FE_SPACE *  	est::const_fe_space;
//const  FE_SPACE *  	const_fe_space;12.3.reiner Versuch

REAL est::get_el_est(EL *el, int comp) const
{
  REAL err_est;
  if (IS_LEAF_EL(el))
    {
      (const_fe_space->bas_fcts->get_real_vec)(el, err_estimates[comp], &err_est);
      return(err_est);
    }
  else
    return(0.0);
}

REAL * est::rw_el_est(EL const*el, int comp)
{
  static const DOF *(*get_dof)(const EL *, const DOF_ADMIN *, DOF *)
    = const_fe_space->bas_fcts->get_dof_indices;
  
  if (IS_LEAF_EL(el))
    {
      const DOF *dof = (*get_dof)(el, const_fe_space->admin, nil);
      return(&err_estimates[comp]->vec[*dof]);
    }
  else
    return(nil);
}

REAL *est::rw_el_estc(EL const *el, int comp)
{
  if (IS_LEAF_EL(el))
    {
      static const DOF *(*get_dof)(const EL *, const DOF_ADMIN *, DOF *)
	= const_fe_space->bas_fcts->get_dof_indices;
      const DOF *dof = (*get_dof)(el, const_fe_space->admin, nil);

      return(&err_c_estimates[comp]->vec[*dof]);
    }
  else
    return(nil);
}

void est::clear_indicator_fct(const EL_INFO *el_info)
{
     static const DOF *(*get_dof)(const EL *, const DOF_ADMIN *, DOF *)
       = const_fe_space->bas_fcts->get_dof_indices;
       const DOF *dof = (*get_dof)(el_info->el, const_fe_space->admin, nil);
   for (int comp=0; comp<ncomps; comp++)
   {
        err_estimates[comp]->vec[*dof] =0.0;
        err_c_estimates[comp]->vec[*dof] = 0.0;
   }
  el_info->el->mark = 1;
  return;
}

void init_err_dof_admin(MESH *mesh)
{
  FUNCNAME("init_err_dof_admin");
  FLAGS periodic = 0U, flag = 0U;
  GET_PARAMETER(2, "periodic", "%d", &periodic);
  if (periodic) 	{flag = ADM_PERIODIC ;}
	else 				{flag = 0U;}
flag = 0U;//wegen bug ueberbuegelt
  const BAS_FCTS  *lagrange= get_lagrange(mesh->dim,0); 
  est::const_fe_space = get_fe_space(mesh, lagrange->name, nil, lagrange, flag);
//Punkte auf dem Bazyzentrum sind von Periodizitat nicht betroffen. oder doch?
  return;
}

#if 0
ohne periodizitaet
void init_err_dof_admin(MESH *mesh)
{
  FUNCNAME("init_err_dof_admin");
  const BAS_FCTS  *lagrange= get_lagrange(0); 
  est::const_fe_space = get_fe_space(mesh, lagrange->name, nil, lagrange);
  return;
}
#endif

