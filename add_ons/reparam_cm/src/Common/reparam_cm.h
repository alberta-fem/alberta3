
/*--------------------------------------------------------------------------*/
/* Routines provided are based on                                           */
/*                                                                          */
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.alberta-fem.de                                               */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/* author of this software:                                                 */
/*   Bjoern Stinner                                                         */
/*   Mathematics Institute                                                  */
/*   University of Warwick, UK                                              */
/*                                                                          */
/*--------------------------------------------------------------------------*/

#ifndef _REPARAM_CM_H_
#define _REPARAM_CM_H_ 1

#include <alberta.h>

/****************************************************************************************************/
/*                                                                                                  */
/* reparam_cm (y_dh, x_dh, crd)                                                                     */
/* computes a reparameterisation of the surface G_h of sphere topology with vertices x_dh by        */
/* 1. computing a harmonic map phi: G_h -> S^2 (method of Dziuk/Clarenz)                            */
/* 2. projecting the vertices y_dh of a given nice mesh on S^2 to the induced mesh phi(G_h)         */
/* 3. creating a new mesh on G_h by applying the inverse of phi to the projected vertices           */
/* 4. adapting the values of field on G_h                                                           */
/*                                                                                                  */
/****************************************************************************************************/
/*                                                                                                  */
/* Variables:                                                                                       */
/*   x_dh:  coordinate vector of the mesh on G_h, MUST BE PROVIDED                                  */
/*   y_dh:  coordinate vector of a nice mesh on S^2 that is to be transferred to G_h by phi^(-1);   */
/*          both coordinate vectors are based on the same mesh (topology) and finite element space  */
/*          orienation MUST BE such that, given vertices p[0],p[1],p[2] of a triangle, the vector   */
/*          n := (p[2]-p[1]) x (p[0]-p[2]) is external normal w.r.t. the sphere                     */
/*          <=> SCP_DOW (n, p[l]) > 0, l=0,1,2                                                      */
/*          MUST BE PROVIDED                                                                        */
/*   crd:   data for the solver of phi etc., see above; THE USER MUST SET:                          */
/*          (1) the values in crd->ns_data                                                          */
/*          (2) the data crd->spp_gmres_data->spp_oem->tolerance, ...max_iter, ...info, ...restart  */
/*              if gmres is applied                                                                 */
/*          THE USER CAN SET:                                                                       */
/*          (a) drvl:  list of n_drvl DOF_REAL_VECs with scalar fields on G_h                       */
/*                     where the vertex values in the new vertices have to be computed,             */
/*                     may be nil, then set n_drvl=0                                                */
/*          (b) drvdl: list of n_drvdl DOF_REAL_D_VECs that have to adapted in the same way         */
/*                                                                                                  */
/****************************************************************************************************/
/*                                                                                                  */
/* RESTRICTIONS:                                                                                    */
/*   mesh dim: 2                                                                                    */
/*   degree of fe_fcts: 1                                                                           */
/*   G_h of sphere topology                                                                         */
/*   outward orientation of induced mesh on S^2 by y_dh                                             */
/*                                                                                                  */
/****************************************************************************************************/

struct newton_solver_data {
  REAL tolerance;
  int restart, max_iter, info;
};

struct spp_gmres_data {
  OEM_DATA *spp_oem;
};

struct spp_uzawa_data {
  REAL tolerance;            /* data for the uzawa main iteration */
  int max_iter, info;
  REAL tolerance_ell;        /* data for the elliptic solver */
  int max_iter_ell, info_ell;
};

struct reparam_cm_data {
  /* data for the Newton solver */
  int newton_solver;                      /* solver flag for the Newton method to compute phi: */
                                          /* 1: standard Newton method */
  struct newton_solver_data ns_data[1];                               /* data for the Newton method */

  /* data for the solver of the saddle point problems */
  int spp_solver;                         /* solver flag for the method: */
                                          /* 1: GMRES */
                                          /* 2: UZAWA with conjugate search directions */
  struct spp_gmres_data spp_gmres_data[1];       /* data for the gmres method */

  struct spp_uzawa_data spp_uzawa_data[1];       /* data for the uzawa method */

  /* if necessary for other solvers, add further data structures */

  int project_info;         /* informations to be displayed after projection of the y_dh to phi(G_h) */
  int quad_degree_on_S;     /* degree for the quadrature on S for the constraints Mk, k=0,1,2,3 */

  DOF_REAL_VEC **drvl;      /* list of DOF_REAL_VECs with scalar fields on G_h */
                            /* where the vertex values in the new vertices have to be computed */
  int n_drvl;               /* number of VECS in this list */
  DOF_REAL_D_VEC **drdvl;   /* list of DOF_REAL_D_VECs that have to adapted in the same way */
  int n_drdvl;              /* number of VECS in this list */
};

void reparam_cm (const DOF_REAL_D_VEC *y_dh, DOF_REAL_D_VEC *x_dh, const struct reparam_cm_data *crd);

#endif
